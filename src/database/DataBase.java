package database;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.HashMap;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import core.Element;
import core.JadeFyre;

public class DataBase {
	
	//returns true if the user is created
	//returns false if the user cannot be created
	synchronized public static boolean createUser(String userName, String password){
		byte[] salt = genSalt(1024);
		byte[] hash = null;
		try {
			hash = hash(password, salt, 2048, 256);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
		File f = new File("data/users/" + userName + "/");
		if(!f.getParentFile().getParentFile().exists()){f.getParentFile().getParentFile().mkdir();}
		if(!f.getParentFile().exists()){f.getParentFile().mkdir();}
		if(f.exists()){
			return false;
		}
		try {
			f.mkdir();
			File saltFile = new File("data/users/" + userName + "/salt.user");
			File hashFile = new File("data/users/" + userName + "/hash.user");
			DataOutputStream out2 = new DataOutputStream(
					new FileOutputStream(saltFile));
			out2.write(salt);
			out2.close();
			DataOutputStream out3 = new DataOutputStream(
					new FileOutputStream(hashFile));
			out3.write(hash);
			out3.close();
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	//returns null if user does not exist.
	//returns the user's data if the user name exists, and it has the correct password
	synchronized public static String login(String userName, String password){
		File f = new File("data/users/" + userName + "/");
		if(!f.getParentFile().getParentFile().exists()){f.getParentFile().getParentFile().mkdir();}
		if(!f.getParentFile().exists()){f.getParentFile().mkdir();}
		if(!f.exists()){
			return null;
		}
		try {
			File saltFile = new File("data/users/" + userName + "/salt.user");
			File hashFile = new File("data/users/" + userName + "/hash.user");
			byte[] salt = new byte[(int)saltFile.length()];
			DataInputStream in2 = new DataInputStream(
					new FileInputStream(saltFile));
			in2.read(salt);
			in2.close();
			
			byte[] hash = new byte[(int)hashFile.length()];
			DataInputStream in3 = new DataInputStream(
					new FileInputStream(hashFile));
			in3.read(hash);
			in3.close();
			byte[] generatedHash = null;
			try {
				generatedHash = hash(password, salt, 2048, 256);
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (InvalidKeySpecException e) {
				e.printStackTrace();
			}
			int index = 0;
			boolean notEqual = false;
			while(index < hash.length && !notEqual){
				if(hash[index] != generatedHash[index]){
					notEqual = true;
				}
				index ++;
			}
			if(notEqual){
				JadeFyre.writeLine("Inncorrect Password");
				return null;
			}
			
			return "sucessful";//change later
		} catch (FileNotFoundException e) {
			return null;
		} catch (IOException e) {
			return null;
		}
	}
	
	private static byte[] hash(String text, byte[] salt, int iterations, int keyLength)throws NoSuchAlgorithmException, InvalidKeySpecException{
		KeySpec spec = new PBEKeySpec(text.toCharArray(), salt, iterations, keyLength);
		SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		byte[] hash = f.generateSecret(spec).getEncoded();
		return hash;
	}
	
	private static byte[] genSalt(int numBytes){
		byte[] salt = new byte[numBytes];
		SecureRandom random = new SecureRandom();
		random.nextBytes(salt);
		return salt;
	}
	
	public static BigInteger getPlayerId(String userName){
		try {
			if(!(new File("data/users/" + userName + "/id.user").exists())){
				return null;
			}
			ObjectInputStream in = new ObjectInputStream(new FileInputStream("data/users/" + userName + "/id.user"));
			BigInteger id = (BigInteger)in.readObject();
			in.close();
			return id;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static boolean setPlayerId(String userName, BigInteger id){
		try {
			File f = new File("data/users/" + userName + "/id.user");
			if(!f.exists()){
				f.createNewFile();
			}
			ObjectOutputStream in = new ObjectOutputStream(new FileOutputStream("data/users/" + userName + "/id.user"));
			in.writeObject(id);
			in.close();
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static boolean worldSaveExists(){
		return new File("data/world/worldsave.wrld").exists();
	}
	
	public static boolean saveWorld(){
		File f = new File("data/");
		if(!f.exists())
			f.mkdir();
		f = new File("data/world/");
		if(!f.exists())
			f.mkdir();
		try {
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("data/world/worldsave.wrld"));
			out.writeObject(Element.getRoot());
			out.close();
			out = new ObjectOutputStream(new FileOutputStream("data/world/spawn_id.wrld"));
			out.writeObject(Element.getSpawn().getID());
			out.close();
			out = new ObjectOutputStream(new FileOutputStream("data/world/id_cntr.wrld"));
			out.writeObject(Element.getCounter());
			out.close();
			out = new ObjectOutputStream(new FileOutputStream("data/world/id_one.wrld"));
			out.writeObject(new BigInteger("1"));
			out.close();
			out = new ObjectOutputStream(new FileOutputStream("data/world/look_up.wrld"));
			out.writeObject(Element.getLookup());
			out.close();
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static boolean loadWorld(){
		File f = new File("data/");
		if(!f.exists())
			f.mkdir();
		f = new File("data/world/");
		if(!f.exists())
			f.mkdir();
		try {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream("data/world/worldsave.wrld"));
			Element.setRoot((Element)in.readObject());
			in.close();
			in = new ObjectInputStream(new FileInputStream("data/world/spawn_id.wrld"));
			BigInteger spawnId = (BigInteger)in.readObject();
			Element.setSpawn(spawnId);
			in.close();
			in = new ObjectInputStream(new FileInputStream("data/world/id_cntr.wrld"));
			Element.setCounter((BigInteger)in.readObject());
			in.close();
			in = new ObjectInputStream(new FileInputStream("data/world/id_one.wrld"));
			Element.setOne((BigInteger)in.readObject());
			in.close();
			in = new ObjectInputStream(new FileInputStream("data/world/look_up.wrld"));
			@SuppressWarnings("unchecked")
			HashMap<BigInteger, Element> readObject = ((HashMap<BigInteger, Element>)in.readObject());
			Element.setLookup(readObject);
			in.close();
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return false;
	}
}