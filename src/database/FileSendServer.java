package database;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class FileSendServer {
	private static int idCntr = Integer.MIN_VALUE;
	private final int ID;
	private PrintWriter out;
	
	public FileSendServer(Socket s, String filePath){
		ID = idCntr;
		idCntr ++;
		try {
			out = new PrintWriter(s.getOutputStream());
		} catch (IOException e) {
			
		}
	}
	
	public void removeSelf(){
		
	}
	
	public void close(){
		out.close();
	}
}
