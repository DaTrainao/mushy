package database;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import core.JadeFyre;

public class FileTransferServerManager implements Runnable{
	
	public static ServerSocket svr;
	private static final int PORT = 1339;
	private static int maxUsers = 10;
	private static int minPortNum = PORT +1;
	private static int maxPortNum = minPortNum + maxUsers;
	private static Thread t;
	private static FileSendServer[] servers = new FileSendServer[maxUsers];
	
	public static void init(){
		try {
			svr = new ServerSocket(PORT);
			t = new Thread(new FileTransferServerManager());
		} catch (IOException e) {
			JadeFyre.log("Failed to initialize FileTransferServerManager's server", JadeFyre.SEVERE);
		}
	}
	
	public static void close(){
		try {
			t.interrupt();
			svr.close();
		} catch (IOException e) {
			JadeFyre.log("Failed to close FileTransferServerManager's server", JadeFyre.SEVERE);
		}
	}
	
	private static void attepmtToManageSocket(Socket s){
		
	}
	
	@Override
	public void run() {
		while(true){
			try {
				Socket s = svr.accept();
				attepmtToManageSocket(s);
				Thread.sleep(0);
			} catch (InterruptedException e) {
				break;
			} catch (IOException e) {
				JadeFyre.log("Socket Error in FileTransferServerManager, probably bad client", JadeFyre.DEBUG);
			}
		}
	}
}