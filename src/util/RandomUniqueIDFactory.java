package util;

import java.util.UUID;

public class RandomUniqueIDFactory {
	
	public static UniqueID createNewUniqueID(){
		
		return new UniqueID(){
			private UUID id = UUID.randomUUID();
			@Override
			public boolean equals(Object obj) {
				return id.equals(obj);
			}
			
			public String toString(){
				return id.toString();
			}
			
		};
	}
}