package util;

public interface UniqueID {
	public boolean equals(Object obj);
	public String toString();
}