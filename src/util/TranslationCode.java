package util;

import java.awt.event.ActionEvent;
import java.math.BigInteger;
import java.util.ArrayList;
import core.Action;
import core.Element;
import core.JadeFyre;

public class TranslationCode {
	
	
	public static BigInteger init_room(String name, String desc) {
		Element e = new Element(Element.getRoot());
		e.setName(name);
		e.addAttribute("desc", desc);
		Element.getRoot().addSub(e);
		return e.getID();
	}
	
	public static BigInteger init_obj(String name, String desc, BigInteger room, String[] attr, String[] attrVals) {
		Element e = new Element(Element.getRoot());
		e.setName(name);
		e.addAttribute("desc", desc);
		for(int k = 0; k < attr.length; k++)
			e.addAttribute(attr[k], attrVals[k]);
		Element.getById(room).addSub(e);
		return e.getID();
	}
	
	public static void setSpawn(BigInteger id){
		Element.setSpawn(Element.getById(id));
	}
	
	public static void setElevationOfRoom(BigInteger id,double elevation){
		Element.getById(id).attr("elevation", new Double(elevation));
	}
	
	public static BigInteger init_obj(String name, String desc, BigInteger room) {
		Element e = new Element(Element.getById(room));
		e.setName(name);
		e.addAttribute("desc", desc);
		Element.getById(room).addSub(e);
		return e.getID();
	}
	
	
	public static void init_exit(final BigInteger room1, final BigInteger room2, String name1, String name2) {
		// TODO Write some damn code.
		Element exitE = new Element(Element.getById(room1));
		Element exitF = new Element(Element.getById(room2));
		
		exitE.setName(name1);
		exitF.setName(name2);
		
		Element.getById(room1).addSub(exitE);
		Element.getById(room2).addSub(exitF);
		exitE.attr("goes_to",room2.toString());
		exitF.attr("goes_to",room1.toString());
		
		exitE.setUseEvent(new Action() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent event) {
				// TODO Auto-generated method stub
				JadeFyre.getCurrentClient().getPlayer().getPlayerElement().moveInto(Element.getById(room2));
				JadeFyre.writeLine("You moved!");
				try{
					JadeFyre.getCommandByName("look").doCmd("");
				}catch(Exception ex){
					
				}
				Element.getById(room1).onLeave(JadeFyre.getCurrentClient().getPlayer().getPlayerElement(), "");
				for(Element e : Element.getById(room1).getSubs()){
					if(!e.equals(JadeFyre.getCurrentClient().getPlayer().getPlayerElement())){
						e.onLeave(JadeFyre.getCurrentClient().getPlayer().getPlayerElement(), "");
					}
				};
				Element.getById(room2).onEnter(JadeFyre.getCurrentClient().getPlayer().getPlayerElement(), "");
				for(Element e : Element.getById(room2).getSubs()){
					if(!e.equals(JadeFyre.getCurrentClient().getPlayer().getPlayerElement())){
						e.onEnter(JadeFyre.getCurrentClient().getPlayer().getPlayerElement(), "");
					}
				};
			}
		});
		exitF.setUseEvent(new Action() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent event) {
				// TODO Auto-generated method stub
				JadeFyre.getCurrentClient().getPlayer().getPlayerElement().moveInto(Element.getById(room1));
				JadeFyre.writeLine("You moved!");
				try{
					JadeFyre.getCommandByName("look").doCmd("");
				}catch(Exception ex){
					
				}
				Element.getById(room2).onLeave(JadeFyre.getCurrentClient().getPlayer().getPlayerElement(), "");
				for(Element e : Element.getById(room2).getSubs()){
					if(!e.equals(JadeFyre.getCurrentClient().getPlayer().getPlayerElement())){
					e.onLeave(JadeFyre.getCurrentClient().getPlayer().getPlayerElement(), "");
					}
				};
				Element.getById(room1).onEnter(JadeFyre.getCurrentClient().getPlayer().getPlayerElement(), "");
				for(Element e : Element.getById(room1).getSubs()){
					if(!e.equals(JadeFyre.getCurrentClient().getPlayer().getPlayerElement())){
						e.onEnter(JadeFyre.getCurrentClient().getPlayer().getPlayerElement(), "");
					}
				}
			};
		});
		
	}
	
	public static BigInteger getObject(Element e, String tar) {
		for(int k = 0; k < e.getAbove().getSubs().size(); k++) 
			if(e.getAbove().getSubs().get(k).getName() == tar)
				return e.getAbove().getSubs().get(k).getID();
		return new BigInteger("-1");
	}
	
	public static BigInteger getObject(String tar) {
		for(int k = 0; k < Element.getRoot().getSubs().size(); k++) 
			if(Element.getRoot().getSubs().get(k).getName() == tar)
				return Element.getRoot().getSubs().get(k).getID();
		return new BigInteger("-1");
	}
	
	public static void init_one_way_exit(final BigInteger room1, final BigInteger room2, String name1) {
		// TODO Write some damn code.
		Element exitE = new Element(Element.getById(room1));
		exitE.attr("goes_to",room2.toString());
		exitE.setName(name1);
		
		Element.getById(room1).addSub(exitE);
		
		exitE.setUseEvent(new Action() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent event) {
				// TODO Auto-generated method stub
				JadeFyre.getCurrentClient().getPlayer().getPlayerElement().moveInto(Element.getById(room2));
				JadeFyre.writeLine("You moved!");
				try{
					JadeFyre.getCommandByName("look").doCmd("");
				}catch(Exception ex){
					
				}
				Element.getById(room1).onLeave(JadeFyre.getCurrentClient().getPlayer().getPlayerElement(), "");
				for(Element e : Element.getById(room1).getSubs()){
					if(!e.equals(JadeFyre.getCurrentClient().getPlayer().getPlayerElement())){
						e.onLeave(JadeFyre.getCurrentClient().getPlayer().getPlayerElement(), "");
					}
				};
				Element.getById(room2).onEnter(JadeFyre.getCurrentClient().getPlayer().getPlayerElement(), "");
				for(Element e : Element.getById(room2).getSubs()){
					if(!e.equals(JadeFyre.getCurrentClient().getPlayer().getPlayerElement())){
						e.onEnter(JadeFyre.getCurrentClient().getPlayer().getPlayerElement(), "");
					}
				};
			}
		});
	}
	
	public static void addCmd(BigInteger id, String e) {
		Object tmp = Element.getById(id).getAttribute("commands");
		if(tmp != null){
			if(tmp instanceof ArrayList<?>){
				ArrayList<String> commands = (ArrayList<String>)tmp;
				commands.add(e);
				Element.getById(id).attr("commands",commands);
			}
			else{
				Element.getById(id).attr("commands",new ArrayList<String>());
			}
		}
		Element.getById(id).attr("commands",new ArrayList<String>());
	}
	
	public static void doCmd(BigInteger id) {
		Object tmp = Element.getById(id).getAttribute("commands");
		ArrayList<String> commands = new ArrayList<String>();
		if(tmp != null){
			if(tmp instanceof ArrayList<?>){
				commands = (ArrayList<String>)tmp;
			}
		}
		for(int k = 0; k < commands.size(); k++) {
			switch(commands.get(k).substring(0, commands.get(k).indexOf(' '))) {
			case "/move":
				move(Element.getById(id), commands.get(k).trim().substring(5, commands.get(k).trim().substring(5).length()));
				break;
			case "/go":
				move(Element.getById(id), commands.get(k).trim().substring(3, commands.get(k).trim().substring(3).length()));
				break;
			case "/say":
				emit(Element.getById(id), commands.get(k).trim().substring(4, commands.get(k).trim().substring(4).length()));
				break;
			case "/emit":
				emit(Element.getById(id), commands.get(k).trim().substring(3, commands.get(k).trim().substring(3).length()));
				break;
			case "/shout":
				emit(Element.getById(id), commands.get(k).trim().substring(6, commands.get(k).trim().substring(6).length()));
				break;
			}
		}
	}
	
	public static void move(Element e, String direction) {
		ArrayList<Element> tmp = e.getAbove().getElementByName(direction);
		if(tmp.size() == 1){
			e.moveInto(Element.getById(new BigInteger((String)tmp.get(0).getAttribute("goes_to"))));
		}
	}
	
	public static void emit(Element e, String data) {
		JadeFyre.writeLineToAll("["+e.getName()+"] "+ data);
	}
	

	public static final void set_default_flags(Element e) {
		e.addAttribute("Flags", "[(AUDIBLE=TRUE),(COLOR=TRUE),(DARK=FALSE),(DEBUG=FALSE),"
				+ "(ENTER_OK=TRUE),(HALT=FLASE),(HAVEN=FALSE),(LIGHT=TRUE),"
				+ "(LINK_OK=FALSE),(NO_COMMAND=TRUE),(NO_WARN=FALSE),(OPAQUE=TRUE),(QUIET=FALSE),"
				+ "(ROYALTY=FALSE),(SAFE=FALSE),(STICKY=FALSE),(TRANSPARENT=FALSE),"
				+ "(UNFINDABLE=FALSE),(VERBOSE=FALSE),(VISUAL=FALSE),(WIZARD=FALSE)]");
	}
	
	public static final void set_default_thing_flags(Element e) {
		e.addAttribute("Flags", "[(AUDIBLE=TRUE),(COLOR=TRUE),(DARK=FALSE),(DEBUG=FALSE),"
				+ "(ENTER_OK=TRUE),(HALT=FLASE),(HAVEN=FALSE),(LIGHT=TRUE),"
				+ "(LINK_OK=FALSE),(NO_COMMAND=TRUE),(NO_WARN=FALSE),(OPAQUE=TRUE),(QUIET=FALSE),"
				+ "(ROYALTY=FALSE),(SAFE=FALSE),(STICKY=FALSE),(TRANSPARENT=FALSE),"
				+ "(UNFINDABLE=FALSE),(VERBOSE=FALSE),(VISUAL=FALSE),(WIZARD=FALSE),"
				+ "(MONITOR=FALSE),(DESTROY_OK=TRUE),(PUPPET=FALSE),(NO_LEAVE=FALSE)]");
	}
	
	public static final void set_default_rooms_flags(Element e) {
		e.addAttribute("Flags", "[(AUDIBLE=TRUE),(COLOR=TRUE),(DARK=FALSE),(DEBUG=FALSE),"
				+ "(ENTER_OK=TRUE),(HALT=FLASE),(HAVEN=FALSE),(LIGHT=TRUE),"
				+ "(LINK_OK=FALSE),(NO_COMMAND=TRUE),(NO_WARN=FALSE),(OPAQUE=TRUE),(QUIET=FALSE),"
				+ "(ROYALTY=FALSE),(SAFE=FALSE),(STICKY=FALSE),(TRANSPARENT=FALSE),"
				+ "(UNFINDABLE=FALSE),(VERBOSE=FALSE),(VISUAL=FALSE),(WIZARD=FALSE),"
				+ "(ABODE=FALSE),(FLOATING=FALSE),(JUMP_OK=FALSE),(MONITOR=FALSE),"
				+ "(Z_TEL=TRUE),(NO_TEL=FALSE),(UNINSPECTED=FALSE)]");
	}
	
	public static final void set_default_exit_flags(Element e) {
		e.addAttribute("Flags", "[(AUDIBLE=TRUE),(COLOR=TRUE),(DARK=FALSE),(DEBUG=FALSE),"
				+ "(ENTER_OK=TRUE),(HALT=FLASE),(HAVEN=FALSE),(LIGHT=TRUE),"
				+ "(LINK_OK=FALSE),(NO_COMMAND=TRUE),(NO_WARN=FALSE),(OPAQUE=TRUE),(QUIET=FALSE),"
				+ "(ROYALTY=FALSE),(SAFE=FALSE),(STICKY=FALSE),(TRANSPARENT=FALSE),"
				+ "(UNFINDABLE=FALSE),(VERBOSE=FALSE),(VISUAL=FALSE),(WIZARD=FALSE),"
				+ "(CLOUDY=FALSE)]");
	}
	
	public static final void set_stats(Element e) {
		e.addAttribute("Strength", 0);
		e.addAttribute("Dexterity", 0);
		e.addAttribute("Constitution", 0);
		e.addAttribute("Intelligence", 0);
		e.addAttribute("Wisdom", 0);
		e.addAttribute("Charisma", 0);
	}
	
	public static void setFlag(BigInteger id, String name, String value) {
		Element e = Element.getById(id);
		String temp = (String) e.getAttribute("Flags");
		if(value.equalsIgnoreCase("true"))
			temp.replaceFirst( (name + "=FALSE" ), (name + "=" + value));
		else
			temp.replaceFirst( (name + "=TRUE" ), (name + "=" + value));
	}
	
	public static String getFlag(Element e, String name) {
		String temp = e.getAttribute("Flags").toString().substring((e.getAttribute("Flags").toString().indexOf(name) + name.length()));
		temp = temp.substring(0, (temp.indexOf(")")) );
		return temp;
	}
} 