package WorldGen;
/*
 * @author Dylan Ball
 * 
 * Holds the arrays for names,colors,gods ect...
 * Can be edited at any time.
 * 
 */
public class CreationResources {
	public static String[] General_Color={"red","blue","green","yellow","orange","black","white","purple","pink","amber"};
	public static String[] Religion_God={"pelor","nerull"};
	public static String[] Nature_metals={"iron","steel","copper","gold","silver"};
	public static String[] General_NeurtalColor={"white","grey","brown","black","dark brown","light brown"};
	public static String[] Nature_TreeLeafDesc= {"swathed in","colored with","shaded by","covered in"};
	public static String[] Organic_HairColor={"black","brown,","chestnut","blond","sand colored","copper colored"};
	public static String[] Nature_gems={"jade","onyx","turquoise","emerald","diamond","sapphire","ruby","garnet","rose quartz","amber","pearl","topaz","opal","amethyst"};
	public static String[] Jewelery_Types={"anklet","armband","bracelet","brooch","circlet","earring","locket","necklace","pendant","ring"};
	public static String[] RootI={"Ag","Al","Alf","Anna","Anor","Aren","Ary","Atha","Era"};
	public static String[] RootII={"cal","can","cane","cel","cor","dale","dor","e","es","eth","for","gan","gon"};
	public static String[] RootIII={"ara","aren","ayth","cir","dia","dua","ela","erla","fora","gath","gith","hisa","ir","ira","la","lor","los","lum"};
	
	
	public static String getlist(String[] list)
	{
		return list[(int) (Math.random()*list.length)];
	}
	
	public static String getname()
	{
		if((int) (Math.random()*50)==0) return "Darius";
		switch((int) (Math.random()*3))
		{
		case 0: return getlist(RootI)+getlist(RootII);
		case 1: return getlist(RootI)+getlist(RootIII);
		case 2: return getlist(RootI)+getlist(RootII)+getlist(RootIII);
		}
		return null;
	}
}
