package WorldGen;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import util.TranslationCode;

public class MiscHelperMethods {

	public static Image getWorldImage(){
		Image img = null;
		try {
	        // retrieve image
	        File inputfile = new File("data/world/WorldImageElevation.png");
	        img = ImageIO.read(inputfile);
	    } catch (IOException e) {
	        //o no!
	    }
		return img;
	}

	public static void Grender(double breakpoint)
	{
		for (int j = 0; j < WorldGenBasic.height; j++)
		{
			for (int k = 0; k < WorldGenBasic.width; k++)
			{
				if(WorldGenBasic.WorldGraph3[j][k]<breakpoint)
				{
					WorldGenBasic.WorldGraph3[j][k]=0;
				}
			}
		}
		MiscHelperMethods.greyWriteImage(WorldGenBasic.WorldGraph3);
	}

	public static void greyWriteImage(double[][] data){
	    //this takes and array of doubles between 0 and 1 and generates a grey scale image from them
	
	    BufferedImage image = new BufferedImage(data.length,data[0].length, BufferedImage.TYPE_INT_RGB);
	
	    for (int y = 0; y < data[0].length; y++)
	    {
	      for (int x = 0; x < data.length; x++)
	      {
	        if (data[x][y]>1){
	            data[x][y]=1;
	        }
	        if (data[x][y]<0){
	            data[x][y]=0;
	        }
	          Color col=new Color((float)data[x][y],(float)data[x][y],(float)data[x][y]); 
	        image.setRGB(x, y, col.getRGB());
	      }
	    }
	
	    try {
	        // retrieve image
	        File outputfile = new File("data/world/WorldImageElevation.png");
	        outputfile.createNewFile();
	
	        ImageIO.write(image, "png", outputfile);
	    } catch (IOException e) {
	        //o no!
	    }
	}

	static void DivideGrid(float x, float y, float width, float height, float c1, float c2, float c3, float c4)
	{
		float Edge1, Edge2, Edge3, Edge4, Middle;
		float newWidth = width / 2;
		float newHeight = height / 2;
	
		if (width > 2 || height > 2)
		{
			Middle = (c1 + c2 + c3 + c4) / 4 + MiscHelperMethods.Displace(newWidth + newHeight);	//Randomly displace the midpoint!
			Edge1 = (c1 + c2) / 2;	//Calculate the edges by averaging the two corners of each edge.
			Edge2 = (c2 + c3) / 2;
			Edge3 = (c3 + c4) / 2;
			Edge4 = (c4 + c1) / 2;
	
			//Make sure that the midpoint doesn't accidentally "randomly displaced" past the boundaries!
			if (Middle < 0)
			{
				Middle = 0;
			}
			else if (Middle > 1.0f)
			{
				Middle = 1.0f;
			}
	
			//Do the operation over again for each of the four new grids.
			DivideGrid( x, y, newWidth, newHeight, c1, Edge1, Middle, Edge4);
			DivideGrid( x + newWidth, y, newWidth, newHeight, Edge1, c2, Edge2, Middle);
			DivideGrid( x + newWidth, y + newHeight, newWidth, newHeight, Middle, Edge2, c3, Edge3);
			DivideGrid( x, y + newHeight, newWidth, newHeight, Edge4, Middle, Edge3, c4);
		}
		else	//This is the "base case," where each grid piece is less than the size of a pixel.
		{
			//The four corners of the grid piece will be averaged and drawn as a single pixel.
			float c = (c1 + c2 + c3 + c4) / 4;
	
			WorldGenBasic.WorldGraph[(int) x][(int) y]=(double) c;
		}
	}

	static float Displace(float num)
	{
		float max = num / (float)(1025 + 1025) * 3;
		return ((float)Math.random() - 0.5f) * max;
	}

	static void DivideGrid2(float x, float y, float width, float height, float c1, float c2, float c3, float c4)
	{
		float Edge1, Edge2, Edge3, Edge4, Middle;
		float newWidth = width / 2;
		float newHeight = height / 2;
	
		if (width > 2 || height > 2)
		{
			Middle = (c1 + c2 + c3 + c4) / 4 + Displace(newWidth + newHeight);	//Randomly displace the midpoint!
			Edge1 = (c1 + c2) / 2;	//Calculate the edges by averaging the two corners of each edge.
			Edge2 = (c2 + c3) / 2;
			Edge3 = (c3 + c4) / 2;
			Edge4 = (c4 + c1) / 2;
	
			//Make sure that the midpoint doesn't accidentally "randomly displaced" past the boundaries!
			if (Middle < 0)
			{
				Middle = 0;
			}
			else if (Middle > 1.0f)
			{
				Middle = 1.0f;
			}
	
			//Do the operation over again for each of the four new grids.
			DivideGrid2( x, y, newWidth, newHeight, c1, Edge1, Middle, Edge4);
			DivideGrid2( x + newWidth, y, newWidth, newHeight, Edge1, c2, Edge2, Middle);
			DivideGrid2( x + newWidth, y + newHeight, newWidth, newHeight, Middle, Edge2, c3, Edge3);
			DivideGrid2( x, y + newHeight, newWidth, newHeight, Edge4, Middle, Edge3, c4);
		}
		else	//This is the "base case," where each grid piece is less than the size of a pixel.
		{
			//The four corners of the grid piece will be averaged and drawn as a single pixel.
			float c = (c1 + c2 + c3 + c4) / 4;
	
			WorldGenBasic.WorldGraph2[(int) x][(int) y]=(double) c;
		}
	}

	public static boolean checkborderclear(double breakpoint)
	{
		for (int j = 0; j < WorldGenBasic.height; j++)
		{
			for (int k = 0; k < WorldGenBasic.width; k++)
			{
				if(j==0||k==0||j==WorldGenBasic.height-1||k==WorldGenBasic.width-1)
				{
					if(WorldGenBasic.WorldGraph3[j][k]>=breakpoint)
					{
						return false;
					}
				}
			}
		}
		return true;
	}

	public static void exits(double breakpoint, int cellcount)
	{
		int complete=0;
		for (int j = 0; j < WorldGenBasic.height; j++)
		{
			for (int k = 0; k < WorldGenBasic.width; k++)
			{
				if(WorldGenBasic.WorldGraph3[j][k]>=breakpoint)
				{
					complete++;
					double percent=(double)complete/(double)cellcount;
					if((int) (Math.random()*100)==0)
						System.out.println("Mapping rooms: "+complete+" completed, "+(int) (percent*100)+" percent complete.");
					if(WorldGenBasic.WorldGraph3[j][k+1]>=breakpoint)
					{
						TranslationCode.init_one_way_exit(WorldGenBasic.coords[j][k], WorldGenBasic.coords[j][k+1], "North");
					}
					if(WorldGenBasic.WorldGraph3[j+1][k]>=breakpoint)
					{
						TranslationCode.init_one_way_exit(WorldGenBasic.coords[j][k], WorldGenBasic.coords[j+1][k], "East");
					}
					if(WorldGenBasic.WorldGraph3[j][k-1]>=breakpoint)
					{
						TranslationCode.init_one_way_exit(WorldGenBasic.coords[j][k], WorldGenBasic.coords[j][k-1], "South");
					}
					if(WorldGenBasic.WorldGraph3[j-1][k]>=breakpoint)
					{
						TranslationCode.init_one_way_exit(WorldGenBasic.coords[j][k], WorldGenBasic.coords[j-1][k], "West");
					}
					
				}
			}
		}
	}

}
