package WorldGen;

import java.math.BigInteger;

import util.TranslationCode;
import core.Element;

public class CaveSystem {

	public static void getCaveRm(BigInteger prev,int copynum)
	{
		System.out.println("Cave rm Function called "+WorldGenBasic.timescalled+"times now.");
		WorldGenBasic.timescalled++;
		//Determine the direction of the last room
		
		int prev_dir = 0;
		int alt_dir = 0;
		int alt_dir2 = 0;
		
		Element prev_e=Element.getById(prev);
		int found=0;
		for(Element e : prev_e.getSubs()){
			if(e.getName().equalsIgnoreCase("North") && found==copynum){
				prev_dir=1;
				alt_dir=0;
				alt_dir2=3;
				break;
			}
			else
			{
				found++;
			}
			if(e.getName().equalsIgnoreCase("East") && found==copynum){
				prev_dir=3;
				alt_dir=2;
				alt_dir2=0;
				break;
			}
			else
			{
				found++;
			}
			if(e.getName().equalsIgnoreCase("South") && found==copynum){
				prev_dir=0;
				alt_dir=1;
				alt_dir2=2;
				break;
			}
			else
			{
				found++;
			}
			if(e.getName().equalsIgnoreCase("West") && found==copynum){
				prev_dir=2;
				alt_dir=3;
				alt_dir2=1;
				break;
			}
			else
			{
				found++;
			}
		}
		
		
		String dir1;
		String dir1op;
		String dir2;
		String dir2op;
		
		int pos=(int) (Math.random()*4);
		if(pos==prev_dir)
		{
			pos=alt_dir;
		}
		
		int pos3=(int) (Math.random()*4);
		if(pos3==prev_dir || pos3==pos)
		{
			pos3=alt_dir2;
		}
		
		int pos2 = 0;
		int pos4 = 0;
		if(pos==0) pos2=1;
		if(pos==1) pos2=0;
		if(pos==2) pos2=3;
		if(pos==3) pos2=2;
		
		if(pos3==0) pos4=1;
		if(pos3==1) pos4=0;
		if(pos3==2) pos4=3;
		if(pos3==3) pos4=2;
		
		dir1=WorldGenBasic.dirs[pos];
		dir1op=WorldGenBasic.dirs[pos2];
		dir2=WorldGenBasic.dirs[pos3];
		dir2op=WorldGenBasic.dirs[pos4];
		
		switch((int) (Math.random()*13) ) //       Main Room Switch
		{
			
			
			case 0: //Normal
			{
				BigInteger Cave_RM_Id=TranslationCode.init_room("The Cave","You see the rough stone walls streching and contorting down the passageway.");
				TranslationCode.init_exit(prev, Cave_RM_Id, dir1,dir1op);
				switch((int) (Math.random()*16)){
				case 0:TranslationCode.init_obj("a red mushroom","A small, forest-dwelling fungus that grows in almost any enviroment. Its cap is red, and its stalk is white.",Cave_RM_Id);break;
				case 1:TranslationCode.init_obj("a blue mushroom","A small, forest-dwelling fungus that grows in almost any enviroment. Its cap is blue, and its stalk is white.",Cave_RM_Id);break;
				case 2:TranslationCode.init_obj("a black mushroom","A small, forest-dwelling fungus that grows in almost any enviroment. Its cap is black, and its stalk is grey.",Cave_RM_Id);break;
				case 3:TranslationCode.init_obj("a glowing green mushroom","A small, forest-dwelling fungus that grows in almost any enviroment. Its cap is a gently glowing green, and its stalk is black.",Cave_RM_Id);break;
				case 4:TranslationCode.init_obj("a glowing purple mushroom","A small, forest-dwelling fungus that grows in almost any enviroment. Its cap is a gently glowing purple, and its stalk is black.",Cave_RM_Id);break;
				default:
				}
				getCaveRm(Cave_RM_Id,0);
				break;
			}
				
			case 1: //Downward slope
			{
				BigInteger Cave_RM_Id=TranslationCode.init_room("The Cave","The cave walls veer downward, forming a smooth slope into the darkness. It looks like you could get back up the slope.");
				TranslationCode.init_exit(prev, Cave_RM_Id, dir1,dir1op);
				getCaveRm(Cave_RM_Id,0);
				break;
			}
				
			case 2: //Steep slope
			{
				BigInteger Cave_RM_Id=TranslationCode.init_room("The Cave","The cave walls veer shaply downward, forming a smooth slope into the darkness. It doesn't look like you could climb back up the slope.");
				TranslationCode.init_one_way_exit(prev, Cave_RM_Id, dir1);
				getCaveRm(Cave_RM_Id,0);
				break;
			}
				
			case 3: //Narrow passage
			{
				BigInteger Cave_RM_Id=TranslationCode.init_room("The Cave","The cave walls veer inward, making for a tight fit.");
				TranslationCode.init_exit(prev, Cave_RM_Id, dir1,dir1op);
				switch((int) (Math.random()*16)){
				case 0:TranslationCode.init_obj("a red mushroom","A small, forest-dwelling fungus that grows in almost any enviroment. Its cap is red, and its stalk is white.",Cave_RM_Id);break;
				case 1:TranslationCode.init_obj("a blue mushroom","A small, forest-dwelling fungus that grows in almost any enviroment. Its cap is blue, and its stalk is white.",Cave_RM_Id);break;
				case 2:TranslationCode.init_obj("a black mushroom","A small, forest-dwelling fungus that grows in almost any enviroment. Its cap is black, and its stalk is grey.",Cave_RM_Id);break;
				case 3:TranslationCode.init_obj("a glowing green mushroom","A small, forest-dwelling fungus that grows in almost any enviroment. Its cap is a gently glowing green, and its stalk is black.",Cave_RM_Id);break;
				case 4:TranslationCode.init_obj("a glowing purple mushroom","A small, forest-dwelling fungus that grows in almost any enviroment. Its cap is a gently glowing purple, and its stalk is black.",Cave_RM_Id);break;
				default:
				}
				getCaveRm(Cave_RM_Id,0);
				break;
			}
			
			case 4: //Spider Nest Entrance Gate
			{
				BigInteger Cave_RM_Id=TranslationCode.init_room("The Cave","You see a smooth hole in the wall, a contrast from the jagged cave walls.  The inside appears to be covered in some sort of webbing, wrapping all around the tunnel.");
				TranslationCode.init_exit(prev, Cave_RM_Id, dir1,dir1op);
				getCaveRm(Cave_RM_Id,0);
				break;
			}
				
			case 5: //Goblin Camp   IDEA!! smugglers hideout, w/ treasure. Also, mushroom spawns.
			{
				BigInteger Cave_RM_Id=TranslationCode.init_room("The Cave","You see a small fire-pit, with skewered rats on it.  Beneath the rats, you see what looks like human remains, being burned. Smells like chicken.");
				TranslationCode.init_obj("a goblin","You see a vaugely humanoid creature, with green and black mottled skin.",Cave_RM_Id);
				TranslationCode.init_obj("a ugly goblin","You see a vaugely humanoid creature, with green and black mottled skin.",Cave_RM_Id);
				TranslationCode.init_obj("a blackened rat","This rodent has been skewered and roasted above a fire, and smells about as nauseating as you'd imagine.",Cave_RM_Id);
				TranslationCode.init_obj("a charred human skull","This poor soul has a hole in the back of his head, evidence of a spear or javilin.",Cave_RM_Id);
				TranslationCode.init_exit(prev, Cave_RM_Id, dir1,dir1op);
				getCaveRm(Cave_RM_Id,0);
				break;
			}
				
			case 6: //Abandoned Camp 
			{
				BigInteger Cave_RM_Id=TranslationCode.init_room("Abandoned Camp in the cave","You see a small fire-pit, with several bedrolls laid out. It looks like several someones have rested here recently. What happened to them?");
				TranslationCode.init_obj("a bedroll","A cozy looking bedroll made of cotton.",Cave_RM_Id);
				TranslationCode.init_exit(prev, Cave_RM_Id, dir1,dir1op);
				getCaveRm(Cave_RM_Id,0);
				break;
			}
				
				
			case 7: //Abandoned Camp 
			{
				BigInteger Cave_RM_Id=TranslationCode.init_room("Abandoned Camp in the cave","You see a small fire-pit, with several bedrolls laid out. It looks like several someones have rested here recently. What happened to them?");
				TranslationCode.init_obj("a bedroll","A cozy looking bedroll made of cotton.",Cave_RM_Id);
				TranslationCode.init_exit(prev, Cave_RM_Id, dir1,dir1op);
				getCaveRm(Cave_RM_Id,0);
				break;
			}
				
			case 8: //Cavern
			{
				BigInteger Cave_RM_Id=TranslationCode.init_room("The Cave","You see the walls suddenly widen, and you find yourself in a cavern.");
				switch((int) (Math.random()*8)){
				case 0:TranslationCode.init_obj("a red mushroom","A small, forest-dwelling fungus that grows in almost any enviroment. Its cap is red, and its stalk is white.",Cave_RM_Id);break;
				case 1:TranslationCode.init_obj("a blue mushroom","A small, forest-dwelling fungus that grows in almost any enviroment. Its cap is blue, and its stalk is white.",Cave_RM_Id);break;
				case 2:TranslationCode.init_obj("a black mushroom","A small, forest-dwelling fungus that grows in almost any enviroment. Its cap is black, and its stalk is grey.",Cave_RM_Id);break;
				case 3:TranslationCode.init_obj("a glowing green mushroom","A small, forest-dwelling fungus that grows in almost any enviroment. Its cap is a gently glowing green, and its stalk is black.",Cave_RM_Id);break;
				case 4:TranslationCode.init_obj("a glowing purple mushroom","A small, forest-dwelling fungus that grows in almost any enviroment. Its cap is a gently glowing purple, and its stalk is black.",Cave_RM_Id);break;
				default:
				}
				TranslationCode.init_exit(prev, Cave_RM_Id, dir1,dir1op);
				getCaveRm(Cave_RM_Id,0);
				break;
			}
				
			case 9: //Slope down into water
			{
				BigInteger Cave_RM_Id=TranslationCode.init_room("The Cave","You see the walls suddenly slope down, and you hear the sound of water lapping at the rock. It looks like this section of the cave has been flooded.");
				TranslationCode.init_exit(prev, Cave_RM_Id, dir1,dir1op);
				break;
			}
				
			case 10: //Chasm
			{
				BigInteger Cave_RM_Id=TranslationCode.init_room("The Cave","You see the walls suddenly widen, and you find yourself in a cavern. However, there is a huge split in the middle of the room, and it looks like it continues forever.");
				TranslationCode.init_exit(prev, Cave_RM_Id, dir1,dir1op);
				break;
			}
				
			case 11: //Chasm w/ bridge
			{
				BigInteger Cave_RM_Id=TranslationCode.init_room("The Cave","You see the walls suddenly widen, and you find yourself in a cavern. However, there is a huge split in the middle of the room, and it looks like it continues forever. You see a small, dangerous looking wooden bridge that crosses to the other side.");
				TranslationCode.init_exit(prev, Cave_RM_Id, dir1,dir1op);
				getCaveRm(Cave_RM_Id,0);
				break;
			}
				
			case 12: //Junction
			{
				BigInteger Cave_RM_Id=TranslationCode.init_room("The Cave","You find yourself at a three-way junction.");
				TranslationCode.init_exit(prev, Cave_RM_Id, dir1,dir1op);
				getCaveRm(Cave_RM_Id,0);
				getCaveRm(Cave_RM_Id,1);
				break;
			}
				
		}
	}

}
