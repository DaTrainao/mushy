package WorldGen;

// Basic World Formation - {Land/Water,Rivers,Lakes,Biomes,temperature,wind,rainfall}



import java.awt.*;
import java.awt.event.ActionEvent;
import java.math.BigInteger;

import ItemSpawns.ForestSpawns;
import Structures.ForestStructures;
import core.*;
import util.TranslationCode;

public class WorldGenBasic
{
	public static int height = 200;
	public static int width = 200;
	public static double[][] WorldGraph=new double[270][270];
	public static double[][] WorldGraph2=new double[270][270];
	public static double[][] WorldGraph3=new double[height][width];
	public static BigInteger[][] coords=new BigInteger[width][height];
	public double[][] A=new double[2][2];
	public static boolean passed=false;
	public static double testpoint=0.0;
	public static boolean finishedGenerating = false;
	public static boolean finishedLinkng = false;
	public static boolean spawnset = false;


	public static void createworld()
	{
		while(!passed)
		{
		testpoint=0.0;
		passed=true;
		float c1, c2, c3, c4;
		c1 = (float)Math.random();
		c2 = (float)Math.random();
		c3 = (float)Math.random();
		c4 = (float)Math.random();
		MiscHelperMethods.DivideGrid(0, 0, 257 , 257 , c1, c2, c3, c4);
		c1 = (float)Math.random();
		c2 = (float)Math.random();
		c3 = (float)Math.random();
		c4 = (float)Math.random();
		MiscHelperMethods.DivideGrid2(0, 0, 257 , 257 , c1, c2, c3, c4);
		// Start fitting WorldGraph1 & 2 to values between 0.0 and 1.0
		// Find the minimum value
		double min=1000;
		for (int j = 0; j < height; j+=1)
		{
			for (int k = 0; k < width; k+=1)
			{
				if(WorldGraph[j][k]<min)
				{
					min=WorldGraph[j][k];
				}

			}
		}
		// Subtract the minimum from every value
		for (int j = 0; j < height; j+=1)
		{
			for (int k = 0; k < width; k+=1)
			{
				WorldGraph[j][k]=WorldGraph[j][k]-min;
			}
		}
		double min2=1000;
		for (int j = 0; j < height; j+=1)
		{
			for (int k = 0; k < width; k+=1)
			{
				if(WorldGraph2[j][k]<min2)
				{
					min2=WorldGraph2[j][k];
				}
			}
		}
		for (int j = 0; j < height; j+=1)
		{
			for (int k = 0; k < width; k+=1)
			{
				WorldGraph2[j][k]=WorldGraph2[j][k]-min2;
			}
		}
		double max=0;
		for (int j = 0; j < height; j+=1)
		{
			for (int k = 0; k < width; k+=1)
			{
				if(WorldGraph[j][k]>max)
				{
					max=WorldGraph[j][k];
				}
			}
		}

		for (int j = 0; j < height; j+=1)
		{
			for (int k = 0; k < width; k+=1)
			{
				WorldGraph[j][k]=WorldGraph[j][k]/max;
			}
		}
		double max2=0;
		for (int j = 0; j < height; j+=1)
		{
			for (int k = 0; k < width; k+=1)
			{
				if(WorldGraph2[j][k]>max2)
				{
					max2=WorldGraph2[j][k];
				}
			}
		}
		for (int j = 0; j < height; j+=1)
		{
			for (int k = 0; k < width; k+=1)
			{
				WorldGraph2[j][k]=WorldGraph2[j][k]/max2;
			}
		}


		// Flatten values to lower 1/10 of their value

		for (int j = 0; j < height; j+=1)
		{
			for (int k = 0; k < width; k+=1)
			{
				if(WorldGraph2[j][k]<0.1)
					WorldGraph2[j][k]=0.0;
				if(WorldGraph2[j][k]>0.1&&WorldGraph2[j][k]<0.2)
					WorldGraph2[j][k]=0.1;
				if(WorldGraph2[j][k]>0.2&&WorldGraph2[j][k]<0.3)
					WorldGraph2[j][k]=0.2;
				if(WorldGraph2[j][k]>0.3&&WorldGraph2[j][k]<0.4)
					WorldGraph2[j][k]=0.3;
				if(WorldGraph2[j][k]>0.4&&WorldGraph2[j][k]<0.5)
					WorldGraph2[j][k]=0.4;
				if(WorldGraph2[j][k]>0.5&&WorldGraph2[j][k]<0.6)
					WorldGraph2[j][k]=0.5;
				if(WorldGraph2[j][k]>0.6&&WorldGraph2[j][k]<0.7)
					WorldGraph2[j][k]=0.6;
				if(WorldGraph2[j][k]>0.7&&WorldGraph2[j][k]<0.8)
					WorldGraph2[j][k]=0.7;
				if(WorldGraph2[j][k]>0.8&&WorldGraph2[j][k]<0.9)
					WorldGraph2[j][k]=0.8;
				if(WorldGraph2[j][k]>0.9&&WorldGraph2[j][k]<1.0)
					WorldGraph2[j][k]=0.9;
			}
		}

		for (int j = 0; j < height; j+=1)
		{
			for (int k = 0; k < width; k+=1)
			{
				if(WorldGraph[j][k]<0.1)
					WorldGraph[j][k]=0.0;
				if(WorldGraph[j][k]>0.1&&WorldGraph[j][k]<0.2)
					WorldGraph[j][k]=0.1;
				if(WorldGraph[j][k]>0.2&&WorldGraph[j][k]<0.3)
					WorldGraph[j][k]=0.2;
				if(WorldGraph[j][k]>0.3&&WorldGraph[j][k]<0.4)
					WorldGraph[j][k]=0.3;
				if(WorldGraph[j][k]>0.4&&WorldGraph[j][k]<0.5)
					WorldGraph[j][k]=0.4;
				if(WorldGraph[j][k]>0.5&&WorldGraph[j][k]<0.6)
					WorldGraph[j][k]=0.5;
				if(WorldGraph[j][k]>0.6&&WorldGraph[j][k]<0.7)
					WorldGraph[j][k]=0.6;
				if(WorldGraph[j][k]>0.7&&WorldGraph[j][k]<0.8)
					WorldGraph[j][k]=0.7;
				if(WorldGraph[j][k]>0.8&&WorldGraph[j][k]<0.9)
					WorldGraph[j][k]=0.8;
				if(WorldGraph[j][k]>0.9&&WorldGraph[j][k]<1.0)
					WorldGraph[j][k]=0.9;
			}
		}
		for (int j = 0; j < height; j+=1)
		{
			for (int k = 0; k < width; k+=1)
			{
				WorldGraph3[j][k]=(WorldGraph2[j][k])*(WorldGraph[j][k]);
			}
		}

		for (int j = 0; j < 7; j--)
		{
			testpoint+=0.01;
			if(MiscHelperMethods.checkborderclear(testpoint))
				break;
		}
		int cellcount=0;
		for (int j = 0; j < height; j+=1)
		{
			for (int k = 0; k < width; k+=1)
			{
				if(WorldGraph3[j][k]>=testpoint)
					cellcount++;
			}
		}
		if(cellcount<=width*height*0.15)
		{
			passed=false;
			//System.out.println("World failed. Land cells:"+ cellcount + ". Making new world...");
		}


		}
		int cellcount=0;
		for (int j = 0; j < height; j+=1)
		{
			for (int k = 0; k < width; k+=1)
			{
				if(WorldGraph3[j][k]>=testpoint)
					cellcount++;
			}
		}
		CreationLib.init_world_species();
		render(testpoint,cellcount);
		finishedGenerating = true;
		System.out.println("World rooms created.");
		MiscHelperMethods.exits(testpoint,cellcount);
		MiscHelperMethods.Grender(testpoint);
		finishedLinkng = true;
		//Grender(testpoint);
		/*
		g.setColor(Color.green);
		g.drawString(Arrays.toString(WorldBiomes),50,50);
		/*
		g.setColor(Color.green);
		g.drawString("Method calls:"+Integer.toString(methodtriggered),50,50);
		g.drawString("cells edited:"+Integer.toString(timestriggered),50,60);
		g.drawString("uncalled coords:"+Integer.toString(uncalled),50,70);
		*/

	}
	public static void render(double breakpoint,int cellcount)
	{
		int moun=0,rain=0,dese=0,fore=0,savv=0,tund=0,beac=0;
		int complete=0;
		double percent=(double)complete/(double)cellcount;
		for (int j = 0; j < height; j++)
		{
			for (int k = 0; k < width; k++)
			{
				if(WorldGraph3[j][k]>=breakpoint)
				{
					String c="";
					if(WorldGraph2[j][k]<=0.4)
					{
						c="Mountains";
						moun++;
					}
					if(WorldGraph2[j][k]>=0.9)
					{
						c="Rainforest";
						rain++;
					}
					if(WorldGraph2[j][k]==0.8)
					{
						c="Desert";
						dese++;
					}
					if(WorldGraph2[j][k]==0.7)
					{
						c="Forest";
						fore++;
					}
					if(WorldGraph2[j][k]==0.6)
					{
						c="Savvanah";
						savv++;
					}
					if(WorldGraph2[j][k]==0.5)
					{
						c="Tundra";
						tund++;
					}
					if(WorldGraph3[j+1][k]<breakpoint)
					{
						c="Beach";
						beac++;
					}
					if(WorldGraph3[j-1][k]<breakpoint)
					{
						c="Beach";
						beac++;
					}
					if(WorldGraph3[j][k+1]<breakpoint)
					{
						c="Beach";
						beac++;
					}
					if(WorldGraph3[j][k-1]<breakpoint)
					{
						c="Beach";
						beac++;
					}
					String name="The "+c;
					String description="";
					if(c.equalsIgnoreCase("forest"))
					{
						switch((int) (Math.random()*2)){
						case 0:description="The wind rustles the bright green leaves of the trees and bushes, and carries pollen, downy seeeds, and the fragrance of wildflowers along with it.";break;
						case 1:description="Red, orange and gold leaves swirl past you on the wind, carrying the fragrance of wildflowers.";break;
						}
					}
					if(c.equalsIgnoreCase("rainforest"))
					{
						description="You are completely surrounded by the vibrant underbrush of the rainforest. Vines and crrepers cover every inch of this place.";
					}
					if(c.equalsIgnoreCase("desert"))
					{
						description="The sand shifts uncertianly beneath your feet, while the sun's fierce heat beats down on you.";
					}
					if(c.equalsIgnoreCase("savvanah"))
					{
						description="You are standing in waist-heigh grass in an unusually flat area, excepting the odd tree.";
					}
					if(c.equalsIgnoreCase("mountains"))
					{
						description="This land is cold and devoid of life, the grass has been replaced with stone, making for uneasy footing.";
					}
					if(c.equalsIgnoreCase("tundra"))
					{
						description="The howling wind blows snow off the ground, making it increasingly difficult to see anything.";
					}
					if(c.equalsIgnoreCase("beach"))
					{
						description="The sun glints off of the bright ribbon of the sea, as you stand on a beach.";
					}
					
					
					
					BigInteger id=TranslationCode.init_room(name,description);
					coords[j][k]=id;
					
					
					if(c.equalsIgnoreCase("forest"))
					{
						switch((int) (Math.random()*2)){
						case 0:Element.getById(id).attr("pm_desc","The leaves of the trees and bushes rustle in the breeze. Crickets and katydids sing throughout the night.");break;
						case 1:Element.getById(id).attr("pm_desc","Raindrops patter against the fresh leaves, and glisten like silver beads on their bright green surfaces. The wet tree trunks have darkened from grey to brown.");break;
						}
						ForestSpawns.add(id);
						ForestStructures.add(id);
					}
					if(c.equalsIgnoreCase("rainforest"))
					{
						Element.getById(id).attr("pm_desc","Raindrops patter against the fresh leaves, and glisten like silver beads on their bright green surfaces. The wet tree trunks have darkened from grey to brown.");
					}
					if(c.equalsIgnoreCase("desert"))
					{
						Element.getById(id).attr("pm_desc","A fierce, cold wind blows the sand around in harsh arcs.");
					}
					if(c.equalsIgnoreCase("savvanah"))
					{
						Element.getById(id).attr("pm_desc","Raindrops patter against the blades of tall grass, making the ground muddy.");
					}
					if(c.equalsIgnoreCase("mountains"))
					{
						Element.getById(id).attr("pm_desc","A fierce, cold wind blows snow from height up the mountain.");
					}
					if(c.equalsIgnoreCase("tundra"))
					{
						Element.getById(id).attr("pm_desc","The howling wind blows snow off the ground, combined with the night, make it impossible difficult to see anything more than a few feet away.");
					}
					if(c.equalsIgnoreCase("beach"))
					{
						Element.getById(id).attr("pm_desc","The moon glints off of the bright ribbon of the sea, as you stand on a beach.");
					}
					
					
					/*
					case 0:TranslationCode.init_obj("a trap","the walls start closing in.",stairs_id);break;
					case 1:TranslationCode.init_obj("a trap","the stairs flatten into a smooth surface.",stairs_id);break;
					case 2:TranslationCode.init_obj("a trap","gas starts coming out of the ceiling.",stairs_id);break;
					case 3:TranslationCode.init_obj("a trap","spikes come out of the walls and stab you ",stairs_id);break;
					case 4:TranslationCode.init_obj("a trap","spears come out of the walls and stab you ",stairs_id);break;
					case 5:TranslationCode.init_obj("a trap","pendelum hallway",stairs_id);break;
					case 6:TranslationCode.init_obj("a trap","falling celing",stairs_id);break;
					case 7:TranslationCode.init_obj("a trap","pit trap(chance of leg breaking)",stairs_id);break;
					case 8:TranslationCode.init_obj("a trap","pit filled with snakes",stairs_id);break;
					case 9:TranslationCode.init_obj("a trap","pit with giant scorpion",stairs_id);break;
					*/
					
					if(c.equalsIgnoreCase("beach"))
					{
						switch((int) (Math.random()*10)){
						case 0:TranslationCode.init_obj("a tan shell","A small shell, washed up on the beach by the tides.",id);break;
						case 1:TranslationCode.init_obj("a conch","A large, spiraling shell, with a light pink colored inside.",id);break;
						case 2:TranslationCode.init_obj("a white shell","A small shell, washed up on the beach by the tides.",id);break;
						case 3:TranslationCode.init_obj("a pearl","A small pearl, washed up on the beach.",id);break;
						case 4:TranslationCode.init_obj("a black shell","A large, black shell, washed up, vacent, on the beach. It looks like something once lived in it.",id);break;
						default:
						}
						
					}
					
					complete++;
					percent=(double)complete/(double)cellcount;
					System.out.println("Creating rooms: "+complete+" completed, "+(int) (percent*100)+" percent complete.");
				}
			}
		}
		System.out.print("Biome Totals[Mountain: "+moun+" ,RainForest: "+rain+" ,Desert: "+dese+" ,Forest: "+fore+" ,Savvanah: "+savv+" ,Tundra: "+tund+" ,Beach: "+beac+" ]");
	}
	/**
	 * @deprecated Use {@link MiscHelperMethods#exits(double,int)} instead
	 */
	public static void exits(double breakpoint, int cellcount)
	{
		MiscHelperMethods.exits(breakpoint, cellcount);
	}
	
	public static String[] dirs={"South","North","West","East"};
	public static int timescalled=0;
	
	/**
	 * @deprecated Use {@link CaveSystem#getCaveRm(BigInteger,int)} instead
	 */
	public static void getCaveRm(BigInteger prev,int copynum)
	{
		CaveSystem.getCaveRm(prev, copynum);
	}
	
	
	/**
	 * @deprecated Use {@link MiscHelperMethods#getWorldImage()} instead
	 */
	public static Image getWorldImage(){
		return MiscHelperMethods.getWorldImage();
	}
	
	/**
	 * @deprecated Use {@link MiscHelperMethods#Grender(double)} instead
	 */
	public static void Grender(double breakpoint)
	{
		MiscHelperMethods.Grender(breakpoint);
	}
	
	/**
	 * @deprecated Use {@link MiscHelperMethods#greyWriteImage(double[][])} instead
	 */
	public static void greyWriteImage(double[][] data){
		MiscHelperMethods.greyWriteImage(data);
	}
	/**
	 * @deprecated Use {@link MiscHelperMethods#DivideGrid(float,float,float,float,float,float,float,float)} instead
	 */
	static void DivideGrid(float x, float y, float width, float height, float c1, float c2, float c3, float c4)
	{
		MiscHelperMethods.DivideGrid(x, y, width, height, c1, c2, c3, c4);
	}
	/**
	 * @deprecated Use {@link MiscHelperMethods#Displace(float)} instead
	 */
	static float Displace(float num)
	{
		return MiscHelperMethods.Displace(num);
	}

	/**
	 * @deprecated Use {@link MiscHelperMethods#DivideGrid2(float,float,float,float,float,float,float,float)} instead
	 */
	static void DivideGrid2(float x, float y, float width, float height, float c1, float c2, float c3, float c4)
	{
		MiscHelperMethods.DivideGrid2(x, y, width, height, c1, c2, c3, c4);
	}
	/**
	 * @deprecated Use {@link MiscHelperMethods#checkborderclear(double)} instead
	 */
	public static boolean checkborderclear(double breakpoint)
	{
		return MiscHelperMethods.checkborderclear(breakpoint);
	}
	
	public static boolean finishedGenerating(){
		return finishedGenerating;
	}
	public static boolean finishedLinking(){
		return finishedLinkng;
	}
	public static boolean finished(){
		return (finishedGenerating && finishedLinkng);
	}
}
