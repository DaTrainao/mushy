package WorldGen;

import java.util.ArrayList;

public class CreationLib
{
	public static ArrayList<String[]> mushroom_races = new ArrayList<String[]>();
	public static ArrayList<String[]> cave_mushroom_races = new ArrayList<String[]>();
	public static ArrayList<String[]> flower_races = new ArrayList<String[]>();
	public static ArrayList<String[]> butterfly_races = new ArrayList<String[]>();
	
	public static void init_world_species()
	{
		mushroom_races.add(MushroomNormal());
		mushroom_races.add(MushroomNormal());
		mushroom_races.add(MushroomSpotted());
		mushroom_races.add(MushroomSpotted());
		mushroom_races.add(MushroomStriped());
		for(int k=0;k<6;k++){
			flower_races.add(FlowerNormal());
		}
		
		for(int k=0;k<6;k++){
			butterfly_races.add(ButterflyNormal());
		}
		
		for(int k=0;k<6;k++){
			cave_mushroom_races.add(MushroomGlowing());
		}
		
	}
	
	public static String[] MushroomNormal()
	{
		String[] Object=new String[2];
		
		String CapColor=CreationResources.getlist(CreationResources.General_Color);
		String StalkColor=CreationResources.getlist(CreationResources.General_NeurtalColor);
		Object[1]="A small, forest-dwelling fungus that grows in almost any enviroment. Its cap is "+CapColor+", and its stalk is "+StalkColor+".";
		Object[0]="a "+CapColor+" mushroom";


		return Object;
	}
	
	public static String[] FlowerNormal()
	{
		String[] Object=new String[2];
		
		String PetalColor=CreationResources.getlist(CreationResources.General_Color);
		String CenterColor=CreationResources.getlist(CreationResources.General_NeurtalColor);
		String PetalNumber=Integer.toString((int) ((Math.random()*8)+4));
		Object[1]="A small, forest-dwelling wildflower. It has "+PetalNumber+" "+PetalColor+" petals and a "+CenterColor+" center.";
		Object[0]="a "+PetalColor+" flower";


		return Object;
	}

	public static String[] MushroomSpotted()
	{
		String[] Object=new String[2];
		
		String CapColor=CreationResources.getlist(CreationResources.General_Color);
		String SpotColor=CreationResources.getlist(CreationResources.General_Color);
		String StalkColor=CreationResources.getlist(CreationResources.General_NeurtalColor);
		
		Object[0]="a "+CapColor+" and "+SpotColor+" spotted mushroom";
		Object[1]="A small, forest-dwelling fungus that grows in almost any enviroment. Its cap is "+CapColor+" with "+SpotColor+" spots, and its stalk is "+StalkColor+".";

		return Object;
	}
	
	public static String[] MushroomGlowing()
	{
		String[] Object=new String[2];
		
		String CapColor=CreationResources.getlist(CreationResources.General_Color);
		String StalkColor=CreationResources.getlist(CreationResources.General_NeurtalColor);
		Object[1]="A small, forest-dwelling fungus that grows in almost any enviroment. Its cap is "+CapColor+", and its stalk is "+StalkColor+". This mushroom emits a soft "+CapColor+" light.";
		Object[0]="a "+CapColor+" mushroom";


		return Object;
	}

	public static String[] MushroomStriped()
	{
		String[] Object=new String[2];
		
		String CapColor=CreationResources.getlist(CreationResources.General_Color);
		String SpotColor=CreationResources.getlist(CreationResources.General_Color);
		String StalkColor=CreationResources.getlist(CreationResources.General_NeurtalColor);
		
		Object[0]="a "+CapColor+" and "+SpotColor+" spotted mushroom";
		Object[1]="A small, forest-dwelling fungus that grows in almost any enviroment. Its cap is "+CapColor+" with "+SpotColor+" spots, and its stalk is "+StalkColor+".";

		return Object;
	}
	
	public static String[] TreeNormal()
	{
		String[] Object=new String[2];

		String TreeLeafDesc=CreationResources.getlist(CreationResources.Nature_TreeLeafDesc);
		String Bark=CreationResources.getlist(CreationResources.General_NeurtalColor);
	
		Object[0]="a tree";
		Object[1]="A large tree, "+TreeLeafDesc+" green leaves. The bark of this tree is "+Bark;
		
		return Object;
	}
	
	public static String[] MonkPelor()
	{
		String[] Object=new String[2];

		String HairColor=CreationResources.getlist(CreationResources.Organic_HairColor);
	
		Object[0]="a monk";
		Object[1]=""
				+ "This monk is wearing plain white robes with the holy emblem of Pelor emblazoned on the back. "
				+ "His hood is pulled down, allowing you to see his "+HairColor+" hair, and his eyes closed tightly in prayer, giving his an aura of tranquillity and reverence."
				+ "He mutters to himself as he prays, seemingly oblivious to your presence.";
		
		return Object;
	}
	
	public static String[] MonkNerull()
	{
		String[] Object=new String[2];

		String HairColor=CreationResources.getlist(CreationResources.Organic_HairColor);
	
		Object[0]="a monk";
		Object[1]=""
				+ "This monk is wearing plain black robes with the holy emblem of Nerull emblazoned on the back. "
				+ "His hood is pulled down, allowing you to see his "+HairColor+" hair, and his eyes closed tightly in prayer, giving his an aura of tranquillity and reverence."
				+ "He mutters to himself as he prays, seemingly oblivious to your presence.";
		
		return Object;
	}
	
	public static String[] NPCloner()
	{
		String[] Object=new String[2];

		String Name=CreationResources.getname();
		String Shirtcolor=CreationResources.getlist(CreationResources.General_Color);
		String Haircolor=CreationResources.getlist(CreationResources.Organic_HairColor);
		String Eyecolor=CreationResources.getlist(CreationResources.General_Color);
		
		String gender= null;
		String pronoun1 = null;
		String pronoun2 = null;
		switch((int) (Math.random()*2))
		{
		case 0:gender="male";pronoun1="his";pronoun2="he";break;
		case 1:gender="female";pronoun1="her";pronoun2="she";break;
		}
	
		Object[0]=Name+", the hermit";
		Object[1]=Name+" bears the marks of "+pronoun1+" solitary residence: slightly hollowed cheeks,a few cuts and bruises, and an air of constant alertness. "+pronoun2+" is wearing a plain "+Shirtcolor+" cloth shirt, with a heavy leather overcoat."
				+pronoun1+" "+Eyecolor+" eyes are framed by well-combed "+Haircolor+" hair. "+pronoun2+" is holding a rather large corssbow in his right hand.";
		
		return Object;
	}
	
	public static String[] NPCadventurer()
	{
		String[] Object=new String[2];

		String Name=CreationResources.getname();
		String Shirtcolor=CreationResources.getlist(CreationResources.General_Color);
		
		String gender= null;
		String pronoun1 = null;
		String pronoun2 = null;
		switch((int) (Math.random()*2))
		{
		case 0:gender="male";pronoun1="his";pronoun2="he";break;
		case 1:gender="female";pronoun1="her";pronoun2="she";break;
		}
	
		Object[0]=Name+", the adventurer";
		Object[1]=Name+" is here, searching for dungeons, with a longsword in a gilded sheath. "+pronoun2+" is wearing a plain "+Shirtcolor+" cloth shirt, under a light chain shirt. "+pronoun2+" is wearing a large backpack, to carry all "+pronoun1+" adventuring gear.";
		
		return Object;
	}
	
	public static String[] NPCthief()
	{
		String[] Object=new String[2];

		String Name=CreationResources.getname();
		String Haircolor=CreationResources.getlist(CreationResources.Organic_HairColor);
		String Eyecolor=CreationResources.getlist(CreationResources.General_Color);
		
		String gender= null;
		String pronoun1 = null;
		String pronoun2 = null;
		switch((int) (Math.random()*2))
		{
		case 0:gender="male";pronoun1="his";pronoun2="he";break;
		case 1:gender="female";pronoun1="her";pronoun2="she";break;
		}
		
		
		
		Object[0]=Name+", the thief";
		Object[1]=Name+" is wearing black leather armour and greaves. "+pronoun2+" also has a wicked-looking dagger in a small, jewelled sheath on "+pronoun1+" side. "
				+ pronoun1+" "+Haircolor+" hair does nothing to hide the averice in "+pronoun1+" "+Eyecolor+" eyes.";
		
		return Object;
	}
	
	public static String[] ButterflyNormal()
	{
		String[] Object=new String[2];

		String wingcolor1=CreationResources.getlist(CreationResources.General_Color);
		String wingcolor2=CreationResources.getlist(CreationResources.General_Color);
		if(wingcolor2.equals(wingcolor1))
		wingcolor2=CreationResources.getlist(CreationResources.General_Color);
	
		Object[0]="a butterfly";
		Object[1]="This brightly-colored "+wingcolor1+" and "+wingcolor2+" butterfly flies around aimlessly.";
		
		return Object;
	}
	
	public static String[] TreasureJewelery()
	{
		String[] Object=new String[2];

		String type=CreationResources.getlist(CreationResources.Jewelery_Types);
		String gem=CreationResources.getlist(CreationResources.Nature_gems);
		String metal=CreationResources.getlist(CreationResources.Nature_metals);
	
		Object[0]="a "+metal+" "+type;
		Object[1]="This "+metal+" "+type+" is set with a small "+gem+" and looks to be quite valuable.";
		
		return Object;
	}
	
}