package commands;

import java.math.BigInteger;
import core.BasicCommand;
import core.Element;
import core.JadeFyre;

public class Mail extends BasicCommand {

	public Mail() {
		super("mail", "Mail : mail <target_id> <message>");
	}

	@Override
	public void doCmd(String vars) {
		String[] temp = {vars.substring(0, vars.indexOf(' ')),vars.substring(vars.indexOf(' '))};
		if(temp.length == 2){
			BigInteger targetId;
			try{
				targetId = new BigInteger(temp[0]);
			}catch(Exception e){
				JadeFyre.writeLine("first parameter not a number");
				return;
			}
			Element targetElement = Element.getById(targetId);
			if(targetElement == null){
				JadeFyre.writeLine("Target does not exist.");
				return;
			}
			int cntr = 0;
			for(Element e : targetElement.getSubs()){
				if(e.getName().startsWith("mail")){
					cntr ++;
				}
			}
			String name = "";
			if(cntr == 0){
				name = "mail";
			}
			else{
				name = "mail_"+cntr;
			}
			Element mail = new Element(targetElement);
			mail.setName(name);
			mail.attr("from", JadeFyre.getCurrentClient().getPlayer().getPlayerElement().toString());
			mail.attr("message", temp[1]);
			targetElement.addSub(mail);
			JadeFyre.writeLine("Mail Sent!");
		}
		else{
			JadeFyre.writeLine("Invalid number of parameters.");
		}
	}

}
