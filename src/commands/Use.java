package commands;

import java.math.BigInteger;
import core.BasicCommand;
import core.Element;
import core.JadeFyre;

public class Use extends BasicCommand {

	public Use() {
		super("use", "use <object_id>");
	}

	@Override
	public void doCmd(String target) {
		BigInteger targetId = null;
		boolean isnumber=true;
		try{
			targetId = new BigInteger(target);
		}
		catch(Exception e){
			isnumber=false;
			//JadeFyre.writeLine("Invalid Syntax (need a number)");
		}
		if(isnumber){
		Element tmp = Element.getById(targetId);
		
		if(JadeFyre.getCurrentClient().getPlayer().getPlayerElement().equals(tmp)){
			JadeFyre.writeLine("Really?!");
			return;
		}
		
		try{
			for(Element e : JadeFyre.getCurrentClient().getPlayer().getPlayerElement().getAbove().getSubs()){
				if(e.equals(tmp)){
					Element.getById(targetId).onUse(JadeFyre.getCurrentClient().getPlayer().getPlayerElement(), target);
					return;
				}
			}
			JadeFyre.writeLine("That is not in the room.");
		}catch(Exception e){
			JadeFyre.writeLine("Inncorrect syntax.");
		}
		
		
		
		
		
		}
		if(!isnumber)
		{
		try{
			for(Element e : JadeFyre.getCurrentClient().getPlayer().getPlayerElement().getAbove().getSubs()){
				if(e.getName().equalsIgnoreCase(target)){
					Element.getById(e.getID()).onUse(JadeFyre.getCurrentClient().getPlayer().getPlayerElement(), target);
					return;
				}
			}
			JadeFyre.writeLine("That is not in the room.");
		}catch(Exception e){
			JadeFyre.writeLine("Inncorrect syntax.");
		}
		}
	}
}