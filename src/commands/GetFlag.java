package commands;

import java.math.BigInteger;

import util.TranslationCode;
import core.BasicCommand;
import core.Element;
import core.JadeFyre;

public class GetFlag extends BasicCommand {

	public GetFlag() {
		super("getflag", "getflag <obj_id> <flag_name>");
	}

	@Override
	public void doCmd(String e) {
		String[] t = e.split(" ");
		JadeFyre.writeLine(TranslationCode.getFlag(Element.getById(BigInteger.valueOf(Integer.parseInt(t[0]))), t[1]));
	}

}