package commands;

import core.BasicCommand;
import core.JadeFyre;

public class SetFormatting extends BasicCommand {

	public SetFormatting() {
		super("format", "types: default-0,darius-1.colors:default-0,red-2,blue-1,green-3. Usage: format <type> <color>");
	}

	@Override
	public void doCmd(String extraInfo) {
		String[] tmp = parseData(extraInfo);
		if(tmp.length == 2){
			JadeFyre.getCurrentClient().getClientProperties().setType(Integer.valueOf(tmp[0]));
			JadeFyre.getCurrentClient().getClientProperties().setCurrentColor(Integer.valueOf(tmp[1]));
		}
	}

}
