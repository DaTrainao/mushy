package commands;

import core.BasicCommand;
import core.Element;
import core.JadeFyre;

public class Move extends BasicCommand {
	
	public Move() {
		super("move", "move <obj_name>");
	}
	
	public void doCmd(String vars) {
		Element e = JadeFyre.getCurrentClient().getPlayer().getPlayerElement();
		Element above = e.getAbove();
		Element[] below = new Element[e.getSubs().size()];
		below = (Element[]) e.getSubs().toArray(below);
		Element[] horiz = new Element[above.getSubs().size()];
		horiz = (Element[]) above.getSubs().toArray(horiz);
		//horiz
		for(Element tmp: horiz){
			if(tmp.toString().equals(vars)){
				e.moveInto(tmp);
				return;
			}
		}
		//below
		for(Element tmp: below){
			if(tmp.toString().equals(vars)){
				e.moveInto(tmp);
				return;
			}
		}
		
		if(above.toString().equals(vars)){
			e.moveInto(above);
			return;
		}
		JadeFyre.writeLine("not found");
	}
}
