package commands;

import java.math.BigInteger;

import core.BasicCommand;
import core.ClientInstance;
import core.Element;
import core.JadeFyre;

public class Say extends BasicCommand {

	public Say() {
		super("say", "say <target_id> <message>");
	}
	
	@Override
	public void doCmd(String extraInfo) {
		if(extraInfo.indexOf(' ') == -1){
			JadeFyre.writeLine("you need a message!");
			return;
		}
		String temp0 = extraInfo.substring(0,extraInfo.indexOf(' '));
		String temp1;
		if(extraInfo.indexOf(' ') + 1 >= extraInfo.length()){
			temp1 = "";
		}
		else{
			temp1 = extraInfo.substring(extraInfo.indexOf(' ') + 1);
		}
		BigInteger targetId;
		try{
			targetId  = new BigInteger(temp0);
		}catch(Exception e){
			JadeFyre.writeLine("First parameter is not a number.");
			return;
		}
		Element tmp = Element.getById(targetId);
		if(tmp != null){
			if(JadeFyre.getCurrentClient().getPlayer().getPlayerElement().equals(tmp)){
				JadeFyre.writeLine("You say \'"+temp1+"\' to yourself. You might be going crazy.");
			}
			for(ClientInstance inst : JadeFyre.getLoggedOnClients()){
				if(inst.getPlayer().getPlayerElement().equals(tmp) && inst.getPlayer().getPlayerElement().getAbove().equals(JadeFyre.getCurrentClient().getPlayer().getPlayerElement().getAbove())){
					inst.writeLine("["+JadeFyre.getCurrentClient().getPlayer().getPlayerElement().toString()+"]"+
							" Tells you \'"+temp1+"\'");
					JadeFyre.writeLine("You tell "+inst.getPlayer().getPlayerElement().toString()+" \'"+temp1+"\'");
					return;
				}
			}
			for(Element tmpE : JadeFyre.getCurrentClient().getPlayer().getPlayerElement().getAbove().getSubs()){
				if(tmpE.equals(tmp)){
					tmp.onSay(JadeFyre.getCurrentClient().getPlayer().getPlayerElement(), temp1);
					return;
				}
			}
			JadeFyre.writeLine("Target is not in this room.");
		}else{
			JadeFyre.writeLine("Could not find target.");
			return;
		}
	}
}