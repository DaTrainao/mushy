package commands;

import java.math.BigInteger;
import java.util.ArrayList;
import core.BasicCommand;
import core.Element;
import core.JadeFyre;

 public class Get extends BasicCommand {
	  
	  public Get() {
		  super("get","get [target_id]");
	  }
	
	  @Override
	  public void doCmd(String target) {
		  BigInteger targetId;
		  try{
			  targetId = new BigInteger(target);
		  }
		  catch(Exception e){
			  JadeFyre.writeLine("Invalid Syntax (need a number)");
			  return;
		  }
		  ArrayList<Element> validElementsToPickUp = JadeFyre.getCurrentClient().getPlayer().getPlayerElement().getAbove().getSubs();
		  for(Element e : validElementsToPickUp){
			  if(e.getID().equals(targetId)){
				  e.moveInto(JadeFyre.getCurrentClient().getPlayer().getPlayerElement());
				  JadeFyre.writeLine("You put \'" + e.getName() + "\' into your inventory.");
				  return;
			  }
		  }
		  JadeFyre.writeLine("Cannot find that");
	  }
 }