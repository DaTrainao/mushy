package commands;

import core.BasicCommand;
import core.Element;
import core.JadeFyre;

public class Make extends BasicCommand {

	public Make() {
		super("make","make <object_name>");
		addAlias("@create");
	}
	
	@Override
	public void doCmd(String vars) {
		if(!vars.equals("")) {
			Element tmpE = new Element(JadeFyre.getCurrentClient().getPlayer().getPlayerElement().getAbove());
			tmpE.addAttribute("name",vars);
			JadeFyre.getCurrentClient().getPlayer().getPlayerElement().getAbove().addSub(tmpE);
			JadeFyre.writeLine("You created '" + vars + "'");
		}
		else{
			JadeFyre.log("Cannot create "+vars+", too few arguments.", JadeFyre.INFO);
		}
	}
}
