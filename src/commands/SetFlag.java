package commands;

import java.math.BigInteger;

import util.TranslationCode;
import core.BasicCommand;

public class SetFlag extends BasicCommand {

	public SetFlag() {
		super("setflag", "setflag <obj_id> <flag_name> <new_value>");
	}

	@Override
	public void doCmd(String e) {
		String[] t = e.split(" ");
		TranslationCode.setFlag(BigInteger.valueOf(Integer.parseInt(t[0])), t[1], t[2]);
	}

}