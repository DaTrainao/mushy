package commands;

import core.BasicServerCommand;
import core.JadeFyre;

public class Logout extends BasicServerCommand{

	public Logout() {
		super("logout", "logout of server");
	}

	@Override
	public void doCmd(String extraInfo) {
		JadeFyre.getCurrentClient().close();
	}
}