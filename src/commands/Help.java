package commands;

import core.BasicCommand;
import core.GenericCommand;
import core.JadeFyre;

public class Help extends BasicCommand {
	
	public Help() {
		super("help", "help [commandName]"); 
	}

	@Override
	public void doCmd(String target) {
		String[] temp = this.parseData(target);
		if(target.equals(""))
			for(GenericCommand k : JadeFyre.getCommands()) 
				JadeFyre.writeLine(k.getName() + "\t | \t" + k.getDescription());
		else
			JadeFyre.writeLine(JadeFyre.getCommandByName(target).toString());
	}
	
}
