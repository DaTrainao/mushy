package commands;

import java.math.BigInteger;

import core.BasicCommand;
import core.Element;
import core.JadeFyre;

public class Attr extends BasicCommand{
	public Attr() {
		super("attr","attr <target_id>:<attribute>=<value>");
		addAlias("@attr");
	}
	
	@Override
	public void doCmd(String in) {
		String[] t = in.split(":");
		if(t.length == 2){
			String[] attr = t[1].split("=");
			if(attr.length == 2){
				String target = t[0];
				String attribute = attr[0];
				String value = attr[1];
				Element e = null;
				try{
					e = JadeFyre.getCurrentClient().getPlayer().getPlayerElement().getAbove().getById(new BigInteger(target));
				}catch(Exception ex){
					//conversion to number error
					JadeFyre.writeLine("inncorrect syntax");
					return;
				}
				
				if(e != null){
					if(!e.addAttribute(attribute, value))
						e.setAttribute(attribute, value);
					JadeFyre.writeLine("You changed '"+attribute+"' of '" +
							target + "' to '"+value+"'.");
					return;
				}
				
			}
		}
		JadeFyre.writeLine("inncorrect syntax.");
	}
}
