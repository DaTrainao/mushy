package commands;

import core.BasicCommand;
import core.JadeFyre;

public class ShoutCommand extends BasicCommand {

	public ShoutCommand() {
		super("shout", "broadcast something to the world.");
	}

	@Override
	public void doCmd(String extraInfo) {
		JadeFyre.writeLineToAll("["+JadeFyre.getCurrentClient().getPlayer().getName()+"] "+extraInfo);
	}

}
