package commands;

import java.math.BigInteger;

import core.BasicCommand;
import core.Element;
import core.JadeFyre;

public class Examine extends BasicCommand{

	public Examine() {
		super("ex", "ex <object_id>");
	}

	@Override
	public void doCmd(String extraInfo) {
		Element e = null;
		try{
			e = JadeFyre.getCurrentClient().getPlayer().getPlayerElement().getAbove().getById(new BigInteger(extraInfo));
		}
		catch(Exception ex){
			JadeFyre.writeLine("Inncorrect Syntax.");
			return;
		}
		if(extraInfo.equals("here")){
			e = JadeFyre.getCurrentClient().getPlayer().getPlayerElement().getAbove();
		}
		if(e != null){
			JadeFyre.writeLine("Attributes:");
			for(Object obj : e.getAttributes())
				JadeFyre.writeLine(obj.toString() + "='"+e.getAttribute(obj.toString())+"'");
			JadeFyre.writeLine("Contains:");
			for(Element name : e.getSubs())
				if(name != null){
					JadeFyre.writeLine(name.toString());
				}
			return;
		}
		JadeFyre.writeLine("no object with that name.");
	}
}