package commands;

import java.math.BigInteger;

import core.BasicCommand;
import core.Element;
import core.JadeFyre;

public class Teleport extends BasicCommand {

	public Teleport() {
		super("tp", "tp [player_name] <target_id>");
	}
	
	public void doCmd(String e) {
		String[] t = e.split(" ");
		if(t[1] == null) {
			Element p = JadeFyre.getCurrentClient().getPlayer().getPlayerElement();
			p.moveInto(Element.getById(BigInteger.valueOf(Integer.parseInt(t[0]))));
		}
		
	}

}