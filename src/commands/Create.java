package commands;

import core.BasicCommand;
import core.Generator;
import core.JadeFyre;
import database.DataBase;
import entities.Player;

public class Create extends BasicCommand {

	private String name;
	private String password;
	
	public Create() {
		super("create", "create <username> <password>");
		// TODO Auto-generated constructor stub
	}
	
	public void doCmd(String vars) {
		String temp[] = this.parseData(vars);
		if(temp.length >= 2) {
			if(temp[0] != null)
				name = temp[0];
			else 
				JadeFyre.writeLine("Incorrect Syntax. Create must be: create <username> <password>");
			
			if(temp[1] != null)
				password = temp[1];
			else 
				JadeFyre.writeLine("Incorrect Syntax. Create must be: create <username> <password>");
			if(DataBase.createUser(name, password)) {
				JadeFyre.writeLine("Successfully create new user with name " + name);
				synchronized(this) {
					Player p = new Player(name, Generator.CreateNewPlayer(name));
					DataBase.setPlayerId(name, p.getPlayerElement().getID());
				}
			} else {
				JadeFyre.writeLine("Username already exists. Please pick another name");
			}
		} else {
			JadeFyre.writeLine("Incorrect Syntax: too few arguments. Create must be: create <username> <password>");
			for(String t : temp) {
				System.out.println(t);
			}
		}
	}
}
