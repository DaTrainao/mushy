package commands;

import core.BasicCommand;
import core.Element;
import core.JadeFyre;

 public class Inv extends BasicCommand {
  
  public Inv() {
   super("inv","inv");        
  }

     @Override
     public void doCmd(String target) {
    	 JadeFyre.writeLine("You have:");
    	 for(Element e : JadeFyre.getCurrentClient().getPlayer().getPlayerElement().getSubs()){
    		 JadeFyre.writeLine(e.toString());
    	 }
     }
 }