package commands;

import core.BasicCommand;
import core.Element;
import core.Generator;
import core.JadeFyre;
import database.DataBase;

public class Connect extends BasicCommand {
	
	private String name;
	private String password;
	
	public Connect() {
		super("connect", "connect <username> <password>");
	}

	@Override
	public void doCmd(String vars) {
		String temp[] = this.parseData(vars);
		if(temp.length == 2){
			if(temp[0] != null)
				name = temp[0];
			else 
				JadeFyre.writeLine("Incorrect Syntax. Create must be: create <username> <password>");
			
			if(temp[1] != null)
				password = temp[1];
			else 
				JadeFyre.writeLine("Incorrect Syntax. Create must be: create <username> <password>");
			
			if(DataBase.login(name, password) == null) {
				JadeFyre.writeLine("Wrong username or password (user may not exist).");
				
			} else {
				String playerData = DataBase.login(name, password);
				synchronized(this) {
					JadeFyre.writeLine("----------");
					JadeFyre.writeLine("-WIZARDRY-");
					JadeFyre.writeLine("----------");
					if(JadeFyre.getCurrentClient().getPlayer().getPlayerElement() == null){
						JadeFyre.getCurrentClient().setPlayer(name,Generator.CreateNewPlayer(name));
					}
					else{
						JadeFyre.writeLine("Already logged in.");
					}
				}
			}
		}
		else{
			JadeFyre.writeLine("wrong number of arguments");
		}
	}
}
