package commands;

import java.math.BigInteger;
import java.util.ArrayList;

import core.BasicCommand;
import core.Element;
import core.JadeFyre;

public class Drop  extends BasicCommand{

	public Drop() {
		super("drop", "drop [object_id]");
	}

	@Override
	public void doCmd(String target) {
		BigInteger targetId;
		  try{
			  targetId = new BigInteger(target);
		  }
		  catch(Exception e){
			  JadeFyre.writeLine("Invalid Syntax (need a number)");
			  return;
		  }
		 ArrayList<Element> validElementsToPickUp = JadeFyre.getCurrentClient().getPlayer().getPlayerElement().getSubs();
		 for(Element e : validElementsToPickUp){
			 if(e.getID().equals(targetId)){
				 e.moveInto(JadeFyre.getCurrentClient().getPlayer().getPlayerElement().getAbove());
				 JadeFyre.writeLine("You dropped \'" + e.getName() + "\'.");
				 return;
			 }
		 }
		 JadeFyre.writeLine("You do not have that");
	}
}