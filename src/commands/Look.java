package commands;

import core.BasicCommand;
import core.Element;
import core.JadeFyre;
import entities.Player;

public class Look extends BasicCommand {
	
	public Look() {
		super("look","look [target_id]");
	}

    @Override
    public void doCmd(String target) {
    	String desc = JadeFyre.getCurrentClient().getPlayer().getPlayerElement().getAbove().getAttribute("desc").toString();
    	if(JadeFyre.getCurrentClient().getPlayer().getPlayerElement().getAbove().getAttribute("pm_desc") != null && !Element.isAM()){
    		desc = JadeFyre.getCurrentClient().getPlayer().getPlayerElement().getAbove().getAttribute("pm_desc").toString();
    	}
    	JadeFyre.writeLine("You are in "+JadeFyre.getCurrentClient().getPlayer().getPlayerElement().getAbove().toString()+", \'"+desc+"\'");
    	if(target.equals("")){
    		for(Element e : JadeFyre.getCurrentClient().getPlayer().getPlayerElement().getAbove().getSubs()){
    			if(e.getAttribute("hidden") == null){
    				JadeFyre.writeLine(e.toString());
    			}
    			else if(!e.equals(JadeFyre.getCurrentClient().getPlayer().getPlayerElement())){
    				if(e.getAttribute("hidden").equals("false")){
    					JadeFyre.writeLine(e.toString());
    				}
    			}
    		}
    	}
    	else{
    		//TODO: do look at
    		JadeFyre.writeLine("wrong params");
    	}
    }
}