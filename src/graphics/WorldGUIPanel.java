package graphics;

import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

import WorldGen.MiscHelperMethods;

public class WorldGUIPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	public WorldGUIPanel(){
		setBounds(0, 0, MiscHelperMethods.getWorldImage().getWidth(null), MiscHelperMethods.getWorldImage().getHeight(null));
	}
	public void paintComponent(Graphics tempG){
		Graphics2D g = (Graphics2D) tempG;
		g.drawImage(MiscHelperMethods.getWorldImage(), 0,0,null);
	}
}