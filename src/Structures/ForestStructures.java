package Structures;

import java.awt.event.ActionEvent;
import java.math.BigInteger;

import ItemSpawns.DungeonSpawns;
import WorldGen.CaveSystem;
import WorldGen.CreationLib;
import util.TranslationCode;
import core.Action;
import core.Element;
import core.JadeFyre;

/*
 * @author Dylan Ball
 * 
 * This file contains the structures that might spawn in a forest.
 * Cannot acess ajacent rooms, will break. Don't try.
 * 
 */

public class ForestStructures {

	public static void add(BigInteger id) {
		switch((int) (Math.random()*8))
		{
		
		case 0: // Small Cabin
		if((int) (Math.random()*15)==0){
			addsmallcabin(id);
		}break;
		
		case 1: // Treehouse
		if((int) (Math.random()*15)==0){
			addtreehouse(id);
		}break;
		
		case 2: // Ruins
		if((int) (Math.random()*15)==0){
			addruins(id);
		}break;
		
		case 3: // Empty Shrine
		if((int) (Math.random()*15)==0){
			addemptyshrine(id);
		}break;
		
		case 4: // Shrine + Monk (pelor)
		if((int) (Math.random()*15)==0){
			addpelorshrine(id);
		}break;
		
		case 5: // Shrine + Monk (nerull)
		if((int) (Math.random()*15)==0){
			addnerullshrine(id);
		}break;
		
		case 6: // Shrine + Monk + Followers (pelor)
		if((int) (Math.random()*15)==0){
			addlargepelorshrine(id);
		}break;
		
		case 7: // Cave System
		if((int) (Math.random()*15)==0){
			addcave(id);
		}break;
		
		
		
		
		
		
		
		}
	}
	
	private static void addcave(BigInteger id) {
		Element.getById(id).attr("name","Cave Entrance in The Forest");
		Element.getById(id).attr("desc","You see the dark, gaping entrance to a cave nestled in between some rocks.");
		BigInteger Cave_RM1_Id=TranslationCode.init_room("The Cave Entrance","You see light from the cave entrance shining into the cave");
		TranslationCode.init_exit(id, Cave_RM1_Id, "Cave Entrance","Cave Exit");
		BigInteger Cave_RM_Id=TranslationCode.init_room("The Cave","You see the rough stone walls streching and contorting down the passageway.");
		TranslationCode.init_exit(Cave_RM1_Id, Cave_RM_Id, "East","West");
		CaveSystem.getCaveRm(Cave_RM_Id, 0);
		
	}

	private static void addlargepelorshrine(BigInteger id) {
		Element.getById(id).attr("name","A Shrine in The Forest");
		Element.getById(id).attr("desc","You see a large statue of the god pelor,and a monk praying at an alter in front of the statue. There are two other robed followers, praying on the ground.");
		TranslationCode.init_obj("a weathered alter","The alter has had it's corners removed by the process of rain and wind. Two candles and a bowl of blood sit upon the alter.",id);
		String[] object=CreationLib.MonkNerull();
		BigInteger monk_id = TranslationCode.init_obj(object[0], object[1],id);
		BigInteger follower1_id = TranslationCode.init_obj("a blue-robed follower", "this man is wearing plain blue robes with the holy emblem of pelor emblazened on the sleeve. He is sitting on the ground, his head bowed in prayer.",id);
		@SuppressWarnings("unused")
		BigInteger follower2_id = TranslationCode.init_obj("a grey-robed follower", "this woman is wearing plain grey robes with the holy emblem of pelor emblazened on the sleeve. She is sitting on the ground, her head bowed in prayer.",id);
		
		
		Element.getById(monk_id).setSayEvent(new Action() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) { switch((int) (Math.random()*3)){
				case 0:JadeFyre.writeLine("The monk says:\"Element.getById(Monk_Id).setSayEvent(new Action())\". You ponder over his wise code."); break;
				case 1:JadeFyre.writeLine("The monk causaually backhands you, and then goes back to his praying."); break;
				case 2:JadeFyre.writeLine("The monk says:\"null\"."); break;
			}}});
		
		
		Element.getById(monk_id).setEnterEvent(new Action() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) { 
				JadeFyre.writeLine("The monk says:\"Welcome to this shrine. May the sun light your path\". and then bows and goes back to praying.");
			}});
		
		Element.getById(follower1_id).setEnterEvent(new Action() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) { 
				JadeFyre.writeLine("The blue robed follower looks up at you and says:\"You have entered a holy place, traveler.\"");
			}});
		
	}

	private static void addnerullshrine(BigInteger id) {
		Element.getById(id).attr("name","A Shrine in The Forest");
		Element.getById(id).attr("desc","You see a large statue of the god nerull,and a solitary monk praying at an alter in front of the statue.");
		Element.getById(id).attr("pm_desc","You see a large statue of the god nerull, lit by flickering candlelight, and a solitary monk praying at an alter in front of the statue.");
		TranslationCode.init_obj("a blood-stained alter","The alter has seen it's fair share of sacrifices, some quite recent judging by the wet blood dripping down the sides of the alter. Two candles and a bowl of blood sit upon the alter.",id);
		TranslationCode.init_obj("a statue of nerull","The statue depicts nerull, a sythe in hand, in the process of smiting the living, like a boss.",id);
		
		String[] object=CreationLib.MonkNerull();
		BigInteger monk_id = TranslationCode.init_obj(object[0], object[1],id);
		
		
		Element.getById(monk_id).setSayEvent(new Action()
		{
			private static final long serialVersionUID = 1L;

		
		public void actionPerformed(ActionEvent e) 
		{ 
			switch((int) (Math.random()*3))
			{
			case 0:JadeFyre.writeLine("The monk says:\"Element.getById(Monk_Id).setSayEvent(new Action())\". You ponder over his wise code."); break;
			case 1:JadeFyre.writeLine("The monk causaually backhands you, and then goes back to his praying."); break;
			case 2:JadeFyre.writeLine("The monk says:\"null\"."); break;
			}
			
		}
		});
		
		Element.getById(monk_id).setEnterEvent(new Action()
		{
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) 
			{ 
				JadeFyre.writeLine("The monk says:\"Welcome to this shrine, traveller. May the darkness hide your steps.\", bows, and goes back to praying.");
			}
			
		});
		
	}

	private static void addpelorshrine(BigInteger id) {
		Element.getById(id).attr("name","A Shrine in The Forest");
		Element.getById(id).attr("desc","You see a large statue of the god pelor,and a solitary monk praying at an alter in front of the statue.");
		Element.getById(id).attr("pm_desc","You see a large statue of the god pelor,lit by flickering candlelight ,and a solitary monk praying at an alter in front of the statue.");
		TranslationCode.init_obj("a weathered alter","The alter has had it's corners removed by the process of rain and wind. Two candles and a bowl of water sit upon the alter.",id);
		TranslationCode.init_obj("a statue of pelor","The statue depicts pelor, with a shining longsword in hand, in the process of smiting undead, like a boss.",id);
		
		String[] object=CreationLib.MonkPelor();
		BigInteger monk_id = TranslationCode.init_obj(object[0], object[1],id);
		
		
		Element.getById(monk_id).setSayEvent(new Action()
		{
			private static final long serialVersionUID = 1L;

		
		public void actionPerformed(ActionEvent e) 
		{ 
			switch((int) (Math.random()*3))
			{
			case 0:JadeFyre.writeLine("The monk says:\"Element.getById(Monk_Id).setSayEvent(new Action())\". You ponder over his wise code."); break;
			case 1:JadeFyre.writeLine("The monk causaually backhands you, and then goes back to his praying."); break;
			case 2:JadeFyre.writeLine("The monk says:\"null\"."); break;
			}
			
		}
		});
		
		Element.getById(monk_id).setEnterEvent(new Action()
		{
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) 
			{ 
				JadeFyre.writeLine("The monk says:\"Welcome to this shrine, traveller. May the sun light your path.\", bows, and goes back to praying.");
			}
			
		});
		
	}

	private static void addemptyshrine(BigInteger id) {
		Element.getById(id).attr("name","A Shrine in The Forest");
		Element.getById(id).attr("desc","You see a large statue of the god pelor,and an abandoned alter. It doesnt look like anyone has been here in a long time.");
		Element.getById(id).attr("pm_desc","You see a large statue of the god pelor,and an abandoned alter. It doesnt look like anyone has been here in a long time.");
		TranslationCode.init_obj("a weathered alter","The alter has had it's corners removed by the process of rain and wind. Two unlit, half-melted candles and an empty bowl sit upon the alter.",id);
		TranslationCode.init_obj("a faceless statue","The statue once honored the god of this shrine, now rendered featureless by years of wind and rain.",id);
	}

	private static void addruins(BigInteger id) {
		Element.getById(id).attr("name","Ruins in The Forest");
		Element.getById(id).attr("desc","You are stand in the ruins of some ancient town. Lumps of fallen white marble litter the ground, and you can see the outline of what used to be a house, now overgrown with trees and grass.");
		switch((int) (Math.random()*3)) { 
		
		case 0: // Varient [cellar]
		BigInteger cellar_id=TranslationCode.init_room("A Small Cellar","The celler is dark and damp, but it is well-concealed and sturdy.");
		TranslationCode.init_exit(id, cellar_id, "Cellar Door","Cellar Door");
		if((int) (Math.random()*2)==0)
			DungeonSpawns.add(cellar_id);
		break;
		
		case 1: // Varient [stairs & cellar]
		BigInteger stairs_id=TranslationCode.init_room("A Staircase","The dark staircase leads to a wooden door.");
		BigInteger cellar_id2=TranslationCode.init_room("A Small Cellar","The celler is dark and damp, but it is well-concealed and sturdy.");
		TranslationCode.init_exit(id, stairs_id, "Cellar Door","Cellar Door");
		TranslationCode.init_exit(stairs_id, cellar_id2, "Wooden Door","Wooden Door");
		if((int) (Math.random()*2)==0)
			DungeonSpawns.add(cellar_id2);
		break;
		
		}
	}

	private static void addtreehouse(BigInteger id) 
	{
		Element.getById(id).attr("name","A Treehouse in The Forest");
		BigInteger cabin_id=TranslationCode.init_room("A Small Treehouse","The interior of the treehouse is warm, and you see an empty fireplace. It looks like someone used to live here.");
		Element.getById(id).attr("pm_desc","You stand at the base of a large tree, with a small treehouse nestled in its branches, and the flickering light of a candle in a window.");
		TranslationCode.init_obj("a nice bed","A small, neatly made bed. It has blue and white covers, and a quilted blanket.",cabin_id);
		TranslationCode.init_obj("an apple","A red fruit.",cabin_id);
		TranslationCode.init_obj("a candle","A small, half-melted wax candle.",cabin_id);
		TranslationCode.init_obj("a table","A wooden table, with a few stains.",cabin_id);
		Element.getById(id).attr("desc","You stand at the base of a large tree, with a small treehouse nestled in its branches.");
		TranslationCode.init_exit(id, cabin_id, "Climb Up","Climb Down");
		
		switch((int) (Math.random()*3)) {
		
		case 0: // Varient [inhabited treehouse - hermit]
		{
			String[] loner=CreationLib.NPCloner();
			BigInteger loner_id=TranslationCode.init_obj(loner[0],loner[1],cabin_id);
			TranslationCode.init_obj("a crossbow","A large crossbow, used to hunt and scare away bandits.",loner_id);
			break;
		}
		
		case 1: // Varient [inhabited threehouse - theif]
		{
			String[] loner=CreationLib.NPCthief();
			BigInteger loner_id=TranslationCode.init_obj(loner[0],loner[1],cabin_id);
			TranslationCode.init_obj("a sharp dagger","A small, but deadly looking dagger.",loner_id);
			break;
		}
		
		}
		
	}

	public static void addsmallcabin(BigInteger id)
	{
		Element.getById(id).attr("name","A Cabin in The Forest");
		Element.getById(id).attr("desc","You are outside a small wooden cabin, with some smoke puffing out of the chimney.");
		Element.getById(id).attr("pm_desc","You stand outside a small wooden cabin, with somke puffing out the chimney, and the flickering light of a candle in a window.");
		BigInteger cabin_id=TranslationCode.init_room("A Small Cabin","The interior of the cabin is warm, and you see a small fire burning in the fireplace. It looks like someone lives here.");
		Element.getById(cabin_id).attr("pm_desc","The interior of the cabin is warm, and you see a small fire burning in the fireplace. It looks like someone lives here.");
		TranslationCode.init_obj("a nice bed","A small, neatly made bed. It has blue and white covers, and a quilted blanket.",cabin_id);
		TranslationCode.init_obj("an apple","A sweet, red fruit.",cabin_id);
		TranslationCode.init_obj("a candle","A small, half-melted wax candle.",cabin_id);
		TranslationCode.init_obj("a card table","A round, wooden table meant for playing cards.",cabin_id);
		TranslationCode.init_exit(id, cabin_id, "Cabin Door","Cabin Door");
		
		switch((int) (Math.random()*3)) {
		
		case 0: // Varient [inhabited cabin - hermit]
		{
			String[] loner=CreationLib.NPCloner();
			BigInteger loner_id=TranslationCode.init_obj(loner[0],loner[1],cabin_id);
			TranslationCode.init_obj("a crossbow","A large crossbow, used to hunt and scare away bandits.",loner_id);
			break;
		}
		
		case 1: // Varient [inhabited cabin - theif]
		{
			String[] loner=CreationLib.NPCthief();
			BigInteger loner_id=TranslationCode.init_obj(loner[0],loner[1],cabin_id);
			TranslationCode.init_obj("a sharp dagger","A small, but deadly looking dagger.",loner_id);
			break;
		}
		
		
		}
	}
	
	
	

}
