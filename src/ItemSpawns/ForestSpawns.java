package ItemSpawns;

import java.math.BigInteger;

import util.TranslationCode;
import WorldGen.CreationLib;

/*
 * @author Dylan Ball
 * 
 * This file holds the random item spawns for the forest biome.
 * 
 */


public class ForestSpawns {

	public static void add(BigInteger id) {
		
		if((int) (Math.random()*4)==0) // Shrooms
		{
			String[] object=CreationLib.mushroom_races.get((int) (Math.random()*CreationLib.mushroom_races.size()));
			TranslationCode.init_obj(object[0],object[1],id);
		}
		
		if((int) (Math.random()*4)==0) // Flowers
		{
			String[] object=CreationLib.flower_races.get((int) (Math.random()*CreationLib.flower_races.size()));
			TranslationCode.init_obj(object[0],object[1],id);
		}
		
		if((int) (Math.random()*8)==0) // Adventurer
		{
			String[] object=CreationLib.NPCadventurer();
			TranslationCode.init_obj(object[0],object[1],id);
		}
		
		if((int) (Math.random()*8)==0) // Theif
		{
			String[] object=CreationLib.NPCthief();
			TranslationCode.init_obj(object[0],object[1],id);
		}
		
		if((int) (Math.random()*4)==0) // Butterfly
		{
			String[] object=CreationLib.butterfly_races.get((int) (Math.random()*CreationLib.butterfly_races.size()));
			TranslationCode.init_obj(object[0],object[1],id);
		}
		
		if((int) (Math.random()*4)==0) // Bee
		{
			TranslationCode.init_obj("a bee","This brightly-colored black and yellow bee flies around aimlessly.",id);
		}
		
		if((int) (Math.random()*4)==0) // Toad
		{
			TranslationCode.init_obj("a toad","A slippery grey and green toad hops along the ground here.",id);
		}
		
		if((int) (Math.random()*8)==0) // Sundial
		{
			TranslationCode.init_obj("a strange sundial","The sundial had two dials, rather than one, and is marked with strange symbols, perhaps a rather unusual system for measuring time?",id);
		}
		
		if((int) (Math.random()*8)==0) // Memorial
		{
			switch((int) (Math.random()*3)){
			case 0:TranslationCode.init_obj("a large, weathered stone slab, embedded in the ground","As you approach the slab, you see that there are names written on the memorial, veterans of a long-forgotten war, judging by the coat of arms on the top of the slab. The names are too badly weathered to read.",id);break;
			case 1:TranslationCode.init_obj("a large, weathered stone slab, embedded in the ground","As you approach the slab, you see that there are names written on the memorial, the victems of some long-forgotten tragedy, judging by the engraved angel on the top of the slab. The names are too badly weathered to read.",id);break;
			case 2:TranslationCode.init_obj("a large, weathered stone slab, embedded in the ground","As you approach the slab, you see that there are names written on the memorial, engineers of some long-forgotten momument, judging by the engraved hammer and chisel on the top of the slab. The names are too badly weathered to read.",id);break;
			}
		}
	}

}
