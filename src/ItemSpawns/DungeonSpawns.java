package ItemSpawns;

import java.math.BigInteger;

import util.TranslationCode;
import WorldGen.CreationLib;

/*
 * @author Dylan Ball
 * 
 * This file holds the random item spawns for dungeon rooms.
 * 
 */

public class DungeonSpawns {
	public static void add(BigInteger id)
	{
		if((int) (Math.random()*4)==0) // Shrooms
		{
			String[] object=CreationLib.cave_mushroom_races.get((int) (Math.random()*CreationLib.cave_mushroom_races.size()));
			TranslationCode.init_obj(object[0],object[1],id);
		}
		
		if((int) (Math.random()*6)==0) // Adamantium spork
		{
			TranslationCode.init_obj("an adamantium spork","Congratz! You're now a REAL adventurer!",id);
		}
		
		switch((int) (Math.random()*6))
		{
		
		case 0:
		TranslationCode.init_obj("a large rat","A large rat, with small red eyes. You should kill it.",id);
		break;
		
		case 1:
		TranslationCode.init_obj("a skeleton","The skeleton is leaning up against the wall.",id);
		TranslationCode.init_obj("a worn book", "The book has a faded black cover with a carving of a dragon.", id);
		break;
		
		case 2:
		String[] object=CreationLib.TreasureJewelery();
		TranslationCode.init_obj(object[0],object[1],id);
		break;
		
		case 3:
		TranslationCode.init_obj("a skeleton","The skeleton is leaning up against the wall.",id);
		String[] object1=CreationLib.TreasureJewelery();
		TranslationCode.init_obj(object1[0],object1[1],id);
		break;
		
		default:break;
		}
	}
}
