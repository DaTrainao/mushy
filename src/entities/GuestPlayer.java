package entities;

public class GuestPlayer extends Player{
	
	//fix unique id generation later
	private static int tmpCntr = 97;
	
	public GuestPlayer() {
		super("Guest",null);
		tmpCntr --;
	}
}