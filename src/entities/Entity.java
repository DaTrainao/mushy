package entities;

import java.util.ArrayList;

import core.Element;

public abstract class Entity implements Runnable {
	private ArrayList<Element> elements = new ArrayList<Element>();
	
	public void run() {
		for(Element k : elements)
			break;
	}
	
	public void registerElement(Element e) {
		elements.add(e);
	}
}