package entities;

import core.Element;

public class Player {
	private final Element player;
	private final String name;
	
	public Player(String name, Element playerElement) {
		player = playerElement;
		this.name = name;
	}
	
	public Element getPlayerElement(){
		return player;
	}

	public String getName() {
		return name;
	}
}