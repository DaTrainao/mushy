package core;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import util.RandomUniqueIDFactory;
import util.UniqueID;
import entities.Player;

public class ClientInstance{
	private DataOutputStream out;
	private BufferedReader in;
	private Socket s;
	private Player player;
	private static ClientSideExtras clientProperties;
	private boolean closed = false;
	private final UniqueID id;
	
	public ClientInstance(Socket s) throws IOException{
		id = RandomUniqueIDFactory.createNewUniqueID();
		this.s = s;
		out = new DataOutputStream(s.getOutputStream());
		in = new BufferedReader(new InputStreamReader(s.getInputStream()));
		player = new Player("guest",null);
		setClientProperties(new ClientSideExtras());
	}
	
	public void writeLine(String text){
		if(text == null){
			JadeFyre.log(this+" error writing",JadeFyre.ERROR);
			return;
		}
		try {
			out.writeBytes(text + "\n");
			JadeFyre.log("Wrote '"+text+"' to " + toString(), JadeFyre.DEBUG);
		} catch (IOException e) {
			closed = true;
			JadeFyre.log("error writing to client "+toString(), JadeFyre.ERROR);
		}
	}
	
	//will return sting or null
	public String readLine(){
		String text = null;
		try {
			text = in.readLine();
		} catch (IOException e) {
			closed = true;
			close();
		}
		return text;
	}
	
	public Socket getSocket(){
		return s;
	}
	
	public Player getPlayer(){
		return player;
	}
	
	public Player setPlayer(String name, Element e) {
		player = new Player(name, e);
		return player;
	}
	
	public void close(){
		try {
			player.getPlayerElement().attr("hidden","true");
			in.close();
			out.close();
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public boolean isClosed(){
		return s.isConnected() || s.isClosed() || closed;
	}
	
	public static ClientSideExtras getClientProperties() {
		return clientProperties;
	}

	public static void setClientProperties(ClientSideExtras clientProperties) {
		ClientInstance.clientProperties = clientProperties;
	}

	public UniqueID getId() {
		return id;
	}
	
	public boolean equals(Object obj){
		return (obj instanceof ClientInstance ? ((ClientInstance)obj).equals(this) : false);
	}
	
	public String toString(){
		return s.getInetAddress().getHostAddress().toString();
	}
}
