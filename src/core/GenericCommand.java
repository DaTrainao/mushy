package core;


//the base for all commands. for a command to execute, it must comply to
//the following String:
//getName() + " blah blah" /*extraInfo*/;

//in other words, it must begin with getName() and extraInfo is anything behind
//it trimmed (take off all leading and ending spaces)


public interface GenericCommand {
	//do the command. extra info is what the user types in after the command
	public void doCmd(String extraInfo);
	
	//returns the command's name
	public String getName();
	
	//returns what the command does
	public String getDescription();
	
	public String[] getAliases();
}