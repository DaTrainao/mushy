package core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;

public class ClientHandler implements Runnable{
	
	private static ArrayList<ClientHandler> clients
		= new ArrayList<ClientHandler>();
	
	private boolean running = false;
	private ClientInstance client;
	private Thread t;
	private static ClientHandler currentlyRunning;
	
	public ClientHandler(ClientInstance c){
		client = c;
		clients.add(this);
		start();
	}
	
	public static boolean stopAll(){
		for(ClientHandler c : clients){
			c.stop();
			clients.remove(c);
			return true;
		}
		return false;
	}
	
	public boolean isDead(){
		return client.isClosed();
	}
	
	public boolean stop(){
		if(running){
			running = false;
			client.close();
			return true;
		}
		return false;
	}
	
	public boolean start(){
		if(!running){
			running = true;
			t = new Thread(this,client.toString());
			t.start();
		}
		return false;
	}
	
	public boolean removeIFDead(){
		if(this.isDead()){
			clients.remove(this);
			this.stop();
			return true;
		}
		return false;
	}
	
	public void writeLine(String text){
		client.writeLine(text);
	}
	
	public static void writeLineToCurrentlyRunning(String text){
		currentlyRunning.getClient().writeLine(text);
	}
	
	public ClientInstance getClient(){
		return client;
	}
	
	public static void writeLineToAll(String text){
		for(ClientHandler c : clients){
			c.writeLine(text);
		}
	}
	
	public static ClientInstance getCurrentClient(){
		return currentlyRunning.getClient();
	}
	
	public static ClientInstance[] getLoggedOnClients(){
		ClientInstance[] c = null;
		try{
			c = new ClientInstance[clients.size()];
			for(int i = 0; i < c.length; i ++){
				c[i] = clients.get(i).getClient();
			}
			c = (ClientInstance[]) Collections.synchronizedCollection(Arrays.asList(c)).toArray();
		} catch(Exception e){
			e.printStackTrace();
		}
		return c;
		
	}
	
	public static void checkAllAndRemoveIfDead(){
		int numRemoved = 0;
		for(ClientHandler c : clients){
			if(c.removeIFDead()){
				numRemoved ++;
			}
		}
		if(numRemoved > 0){
			JadeFyre.log("Removed " + numRemoved + " dead clients.", JadeFyre.INFO);
		}
	}
	
	@Override
	public void run() {
		try{
			JadeFyre.log(client.getSocket().getInetAddress().toString()
					+ " connected.", JadeFyre.INFO);
			String userIn="HI!";
			
			client.writeLine("----------");
			client.writeLine("-JADEFYRE-");
			client.writeLine("----------");
			
			boolean foundCmd = false;
			Iterator<GenericCommand> iter;
			GenericCommand currCmd;
			
			boolean menu = true;
			
			while(menu)
			{
				userIn = client.readLine();
				
				if(userIn == null){
					JadeFyre.log("Null from " + client.toString(), JadeFyre.DEBUG);
					break;
				}
				else{
					synchronized (this) {
						currentlyRunning = this;
						userIn = userIn.trim();
						foundCmd = false;
						iter = (Iterator<GenericCommand>) JadeFyre.getCommands().iterator();
						
							while(!foundCmd && iter.hasNext()){
								currCmd = iter.next();
								
								//command name
								if(userIn.startsWith(currCmd.getName() + " ") || 
									userIn.equals(currCmd.getName())){
										String params = "";
										boolean first = true;
										for(String tmp : userIn.substring(currCmd.getName().length()).trim().split(" ")){
											if(first){
												params += tmp;
												first = false;
											}
											else{
												params += ","+tmp;
											}
										}
										JadeFyre.log(client.toString()+" ran " + 
												currCmd.getName() + "([" +params+"])",JadeFyre.DEBUG);
										if(JadeFyre.getCurrentClient().getPlayer().getPlayerElement() == null && !currCmd.getName().equals("connect") && !currCmd.getName().equals("create")){
							    			JadeFyre.writeLine("Please log in!");
							    		}
										else{
											currCmd.doCmd(
												userIn.substring(currCmd.getName().length()).trim());
										}
										foundCmd = true;
								}
								
								if(!foundCmd){
									//command aliases
									for(String alias : currCmd.getAliases()){
										if(userIn.startsWith(alias + " ") || 
											userIn.equals(alias)){
											currCmd.doCmd(
												userIn.substring(alias.length()).trim());
											foundCmd = true;
										}
									}
								}
							}
							
							if(!foundCmd){
								userIn = userIn.trim();
								userIn += " "; //fixes empty strings and strings with no spaces
								JadeFyre.writeLine("Could not find command \"" + 
									userIn.substring(0, userIn.indexOf(' ')) + "\"");
							}
						}
						Thread.sleep(0);
					}
				}
				
			
			String tmpAddress = client.getSocket().getInetAddress().toString();
			client.close();
			clients.remove(client);
			JadeFyre.log(tmpAddress + " diconnected.",JadeFyre.INFO);
		}
		catch(Exception e){
			e.printStackTrace();
			synchronized (this) {
				client.close();
				clients.remove(client);
			}
		}
	}
}