package core;

public abstract class BasicServerCommand extends BasicCommand {

	public BasicServerCommand(String name, String description) {
		super(name, description);
	}
	
}