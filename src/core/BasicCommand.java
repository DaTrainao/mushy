package core;

import java.util.ArrayList;

public abstract class BasicCommand implements GenericCommand {
	private String name;
	private String description;
	private static ArrayList<String> aliases = new ArrayList<String>();
	
	public BasicCommand(String name, String description) {
		this.name = name;
		this.description = description;
	}
	
	public String getName(){
		return name;
	}
	
	public String getDescription(){
		return description;
	}
	
	public String[] getAliases(){
		return new String[0];
	}
	
	public void addAlias(String alias) {
		this.aliases.add(alias);
	}
	
	public void removeAlias(String alias) {
		for(int k = 0; k < this.aliases.size(); k++) {
			if(this.aliases.get(k).equals(alias))
				this.aliases.remove(k);
		}
	}
	
	protected String getPlayerName(){
		return JadeFyre.getCurrentClient().getPlayer().getName();
	}
	
	public String[] parseData(String vars) {
		vars = vars.trim();
		String[] temp = vars.split(" ");
		return temp;
	}
	
	public boolean equals(Object obj){
		return (obj instanceof GenericCommand ? ((GenericCommand)obj).getName().equals(this.getName()) : false);
	}
	
	public String toString(){
		return getName()+" : "+getDescription();
	}
}