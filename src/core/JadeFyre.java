package core;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;

import WorldGen.WorldGenBasic;
import serverSideCommands.*;
import commands.*;
import database.DataBase;

/*
 * 
 * @author:Chris Colahan
 * @version 2.0
 * 
 */

public class JadeFyre
{
	private static boolean running = false;
	public static final boolean INFO_MESSAGES_ENABLED = true;
	public static final boolean WARNING_MESSAGES_ENABLED = true;
	public static final boolean DEBUG_MESSAGES_ENABLED = true;
	public static final boolean ERROR_MESSAGES_ENABLED = true;
	public static final boolean SEVERE_MESSAGES_ENABLED = true;
	public static final boolean DEATH_MESSAGES_ENABLED = true;
	
	public static final int INFO = 0; //FYI...
	public static final int WARNING = 1; //minor
	public static final int DEBUG = 2; //info for the developer that may slow down server
	public static final int ERROR = 3; //a common error
	public static final int SEVERE = 4; //not death, but perhaps a major feature crashes
	public static final int DEATH = 5; //your program dies...
	
	//add all commands here
	public static void initCmds(){
		addCommand(new Help());
		addCommand(new Look());
		addCommand(new Connect());
		addCommand(new Move());
		addCommand(new Create());
		addCommand(new Make());
		addCommand(new Attr());
		addCommand(new Get());
		addCommand(new Drop());
		addCommand(new Logout());
		addCommand(new Inv());
		addCommand(new Examine());
		addCommand(new Mail());
		addCommand(new Use());
		addCommand(new Say());
		addCommand(new ShoutCommand());
		addCommand(new GetFlag());
		addCommand(new SetFlag());
		addCommand(new GenericCommand() {
			
			@Override
			public String getName() {
				return "rand";
			}
			
			@Override
			public String getDescription() {
				return "random String of characters";
			}
			
			@Override
			public String[] getAliases() {
				return new String[0];
			}
			
			@Override
			public void doCmd(String extraInfo) {
				byte[] b = new byte[new Random().nextInt(18)+3];
				new Random().nextBytes(b);
				String rand = "";
				for(byte tmp : b){
					if(tmp > 126){
						tmp -= 126;
					}
					if(tmp < 33){
						tmp += (33);
					}
					rand += ((char)tmp);
				}
				JadeFyre.writeLine(rand);
			}
		});
	}
	
	public static void initServerSideCommands(){
		addServerCommand(new ListLoggedOn());
		addServerCommand(new Restart());
		addServerCommand(new ShutDown());
		addServerCommand(new HelpServerCommands());
		addServerCommand(new SaveWorld());
		addServerCommand(new LoadWorld());
		addServerCommand(new ListAllWorldObjects());
		addServerCommand(new GUIWorld());
		addServerCommand(new DumpWorld());
	}
	
	public static void initWorld() {
		if(DataBase.worldSaveExists()) {
			DataBase.loadWorld();
		} else {
			Element.getRoot();
			WorldGenBasic.createworld();
			ArrayList<Element> tmp = Element.getRoot().getSubs();
			int pos = new Random().nextInt(tmp.size());
			Element.setSpawn(tmp.get(pos));
			DataBase.saveWorld();
		}
		if(Element.getSpawn() == null){
			JadeFyre.log("There is no spawn room!", SEVERE);
		}
		Element.updateTime();
	}
	
	public static void shutDownWorld(){
		DataBase.saveWorld();
	}
	
	private static ServerSocket svr;
	
	public static HashSet<GenericCommand> commands = new HashSet<GenericCommand>();
	private static HashSet<BasicServerCommand> serverCommands = new HashSet<BasicServerCommand>();
	
	private static Thread mainServerThread;
	private static Thread timeKeeperThread;
	
	public static void main (String args[]) throws IOException
	{
		init(); //initialize server and start stuff
	}
	
	//adds the command if it does not already exist and the name is not same as one that alredy exists
	//returns true if added
	public static boolean addCommand(GenericCommand c){
		boolean contained = false;
		Iterator<GenericCommand> tmp = (Iterator<GenericCommand>) commands.iterator();
		while(!contained && tmp.hasNext()){
			contained = c.getName().equals(tmp.next().getName());
		}
		if(!contained){
			commands.add(c);
			log("Added command \"" + c.getName() + "\"",INFO);
			return true;
		}
		log("Command '" + c.getName() + "' already exists.",JadeFyre.ERROR);
		return false;
	}
	
	public static ClientInstance getPlayerByName(String name){
		ClientInstance[] tmp = getLoggedOnClients();
		ClientInstance theOne = null;
		int i = 0;
		boolean found = false;
		while(!found && i < tmp.length){
			if(tmp[i].getPlayer().getName().equals(name)){
				theOne = tmp[i];
				found = true;
			}
			i ++;
		}
		return theOne;
	}
	
	public static boolean addServerCommand(BasicServerCommand cmd){
		boolean contained = false;
		Iterator<BasicServerCommand> tmp = (Iterator<BasicServerCommand>) serverCommands.iterator();
		while(!contained && tmp.hasNext()){
			contained = cmd.getName().equals(tmp.next().getName());
		}
		
		if(!contained){
			serverCommands.add(cmd);
			log("Added server command \"" + cmd.getName() + "\"",INFO);
			return true;
		}
		return false;
	}
	
	//true if removed
	public static boolean removeCommand(GenericCommand c){
		return commands.remove(c);
	}
	
	public static ArrayList<GenericCommand> getCommands(){
		ArrayList<GenericCommand> tmp = null;
		try{
			tmp = new ArrayList<GenericCommand>(Collections.synchronizedCollection(commands));
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return tmp;
	}
	
	//returns null if not found
	//searches with equalsIgnoreCase
	public static GenericCommand getCommandByName(String commandName){
		boolean found = false;
		Iterator<GenericCommand> iter = (Iterator<GenericCommand>) JadeFyre.getCommands().iterator();
		GenericCommand command = null;
		while(!found && iter.hasNext()){
			GenericCommand c = iter.next();
			if(c.getName().equalsIgnoreCase(commandName)){
				command = c;
				found = true;
			}
		}
		return command;
	}
	
	public static ArrayList<BasicServerCommand> getServerCommands(){
		ArrayList<BasicServerCommand> tmp = null;
		try{
			tmp = new ArrayList<BasicServerCommand>(Collections.synchronizedCollection(serverCommands));
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return tmp;
	}
	
	public static void writeLine(String line){
		ClientHandler.writeLineToCurrentlyRunning(line);
	}
	
	public static void writeLineToAll(String line){
		ClientHandler.writeLineToAll(line);
	}
	
	@Deprecated
	public static void write(String text){
	}
	
	public static ClientInstance getCurrentClient(){
		return ClientHandler.getCurrentClient();
	}
	
	public static ClientInstance[] getLoggedOnClients(){
		return ClientHandler.getLoggedOnClients();
	}
	
	public static void restart(){
		shutDown();
		init();
	}
	
	public static void log(String message, int severity){
		String source = "";
		if(DEATH_MESSAGES_ENABLED){
			String className = Thread.currentThread().getStackTrace()[2].getClassName();
			String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
			String lineNumber = String.valueOf(Thread.currentThread().getStackTrace()[2].getLineNumber());
			source = "["+className+"."+methodName+", line "+lineNumber+"]";
		}
		switch(severity){
			case INFO:
				if(INFO_MESSAGES_ENABLED)
					System.out.println("[INFO]" + message);
				break;
			case WARNING:
				if(WARNING_MESSAGES_ENABLED)
					System.out.println("[WARNING]" + message);
				break;
			case DEBUG:
				if(DEBUG_MESSAGES_ENABLED)
					System.out.println("[DEBUG]" + message + " " + source);
				break;
			case ERROR:
				if(ERROR_MESSAGES_ENABLED)
					System.out.println("[ERROR]" + message);
				break;
			case SEVERE:
				if(SEVERE_MESSAGES_ENABLED)
					System.out.println("[SEVERE]" + message);
				break;
			case DEATH:
				if(DEATH_MESSAGES_ENABLED)
					System.out.println("[DEATH]" + message);
				break;
			default:
				System.out.println("[UNKNOWN]" + message);
				break;
		}
	}
	
	public static void init(){
		
		if(!running){
			running = true;
			initWorld(); //initialize world
			log("[INIT]World initialized.",INFO);
			ServerConsole.start();
			log("[INIT]Started Console.",INFO);
			initCmds(); //client commands
			log("[INIT]Loaded client commands.",INFO);
			initServerSideCommands(); //server commands
			log("[INIT]Loaded server side commands.",INFO);
			try {
				svr = new ServerSocket(1338); //Port on which to run
			} catch (IOException e) {
				log("Port already occupied.",JadeFyre.DEATH);
				System.exit(0);
			}
			log("[INIT]Server bound to port.",INFO);
			mainServerThread = new Thread(new Runnable() {
				
				@Override
				public void run() {
					while(running){
						try {
							Socket s = svr.accept();
							if(s != null){
								new ClientHandler(new ClientInstance(s));
							}
							else{
								log("A socket was null!",JadeFyre.ERROR);
							}
							Thread.sleep(0);
						} catch (IOException e) {
							break;
						} catch (InterruptedException e) {
							JadeFyre.log("Server Interrupted.", JadeFyre.DEBUG);
							break;
						} catch (Exception e){
							break;
						}
					}
				}
			},"MAIN_SERVER_THREAD");
			mainServerThread.start();
			log("[INIT]Server Started",JadeFyre.INFO);
			timeKeeperThread = new Thread(new Runnable() {
				
				@Override
				public void run() {
					while(running){
						try {
							Thread.sleep(1);
							Element.updateTime();
						} catch (InterruptedException e) {
							log("time keeper interrupted",DEBUG);
							break;
						}
					}
					
				}
			}, "TIME_KEEPER");
			timeKeeperThread.start();
			log("[INIT]Time keeper started.",JadeFyre.INFO);
		}
	}
	
	public static void shutDown(){
		if(running){
			log("[SHUTDOWN]Attempting to shutdown.",INFO);
			shutDownWorld();
			System.out.println("[SHUTDOWN]Shut down world.");
			ClientHandler.writeLineToAll("[DICONNECTED BY SERVER]\n");
			ClientHandler.stopAll();
			log("[SHUTDOWN]Disconnected clients.",INFO);
			mainServerThread.interrupt();
			try {
				svr.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			log("[SHUTDOWN]Shut down Main Server.",INFO);
			svr = null;
			running = false;
			timeKeeperThread.interrupt();
			log("[SHUTDOWN]Stopped time keeper.",INFO);
			log("[SHUTDOWN]SUCCESSFULLY SHUTDOWN.",INFO);
		}
		else{
			log("Attempted to shutdown when not running.",WARNING);
		}
	}
	
	public static boolean isRunning(){
		return running;
	}
}