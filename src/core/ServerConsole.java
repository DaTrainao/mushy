package core;

import java.util.Iterator;
import java.util.Scanner;

public class ServerConsole implements Runnable{
	
	private static Thread t = null;
	private static boolean running = false;
	
	public static boolean start(){
		if(!running){
			t = new Thread(new ServerConsole(), "Server_Console");
			t.start();
			running = true;
			return true;
		}
		return false;
	}
	
	public static boolean stop(){
		if(running){
			t.interrupt();
			t = null;
			running = false;
			return true;
		}
		return false;
	}
	
	public void run() {
		Scanner in = new Scanner(System.in);
		Iterator<BasicServerCommand> iter;
		BasicServerCommand currCmd = null;
		String input = "";
		boolean foundCmd;
		
		while(true){
			try {
				input = in.nextLine();
				
				synchronized (this) {
					input = input.trim();
					foundCmd = false;
					iter = (Iterator<BasicServerCommand>)
						JadeFyre.getServerCommands().iterator();
					while(!foundCmd && iter.hasNext()){
						currCmd = iter.next();
						
						//command name
						if(input.startsWith(currCmd.getName() + " ") || input.equals(currCmd.getName())){
							currCmd.doCmd(
								input.substring(currCmd.getName().length()).trim());
							JadeFyre.log("Administrator ran " + currCmd.getName(), JadeFyre.DEBUG);
							foundCmd = true;
						}
						
						if(!foundCmd){
							//command aliases
							for(String alias : currCmd.getAliases()){
								if(input.startsWith(alias + " ") || 
										input.equals(alias)){
									currCmd.doCmd(
											input.substring(alias.length()).trim());
									foundCmd = true;
								}
							}
						}
						
						
					}
					
				}
				if(!foundCmd){
					input = input.trim();
					input += " "; //fixes empty strings and strings with no spaces
					System.out.println("Could not find command \"" + 
							input.substring(0, input.indexOf(' ')) + "\"");
				}
				
				Thread.sleep(0);
			} catch (InterruptedException e) {
				//shutting down
				break;
			}
		}
		
		JadeFyre.log("Server command processing stopped.", JadeFyre.INFO);
		
		in.close();
	}
}
