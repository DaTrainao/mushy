package core;

import java.awt.event.ActionEvent;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;

/*
 * 
 * @author:Christopher Colahan
 * @version 3.0
 * 
 */

public class Element implements Serializable{
	private static final long serialVersionUID = 2L;
	private static Element rootElement = Generator.init();
	private static BigInteger spawn_id;
	private HashMap<String,Serializable> data = new HashMap<String,Serializable>();
	private static BigInteger idCntr;
	private static BigInteger one;
	private final BigInteger id;
	private Element above;
	private ArrayList<Element> sub = new ArrayList<Element>();
	private static HashMap<BigInteger,Element> lookUp;
	private static long currTime = System.currentTimeMillis()/1000;  //in seconds
	private static long worldID;
	private HashMap<Long,Thread> delayedEvents = new HashMap<Long,Thread>();
	private Action useEvent = new Action() {
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent e) {
			JadeFyre.writeLine("Nothing happens...");
		}
	};
	private Action sayEvent = new Action() {
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent e) {
			JadeFyre.writeLine("...no response.");
		}
	};
	private Action enterEvent = new Action() {
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent e) {
		}
	};
	private Action leaveEvent = new Action() {
		private static final long serialVersionUID = 1L;
		@Override
		public void actionPerformed(ActionEvent e) {
			
		}
	};
	
	public Element(Element above){
		if(idCntr == null){
			idCntr = new BigInteger("0");
			one = new BigInteger("1");
			lookUp = new HashMap<BigInteger,Element>();
		}
		this.above = above;
		id = new BigInteger(idCntr.toString());
		idCntr = idCntr.add(one);
		if(lookUp.put(id,this) != null){
			System.out.println("ID ERROR: REWROTE OVER AN EXISTING OBJECT WHEN CREATING A NEW ONE");
		}
		attr("hidden", "false");
	}
	
	public static void setOne(BigInteger i){
		one = i;
	}
	
	public BigInteger getID(){
		return id;
	}
	
	//returns the current 'world' time (in seconds). see System.curTimeMillis() for details. gives UTC time
	//how many seconds since Midnight, Jan. 1st, 1970 UTC
	public static long getCurrTimeSec(){
		return currTime;
	}
	
	//how many minutes since Midnight, Jan. 1st, 1970 UTC
	public static long getCurrTimeMin(){
		return currTime/60;
	}
	
	//how many hours since Midnight, Jan. 1st, 1970 UTC
	public static long getCurrTimeHour(){
		return currTime/(60*60);
	}
	
	//how many days since Midnight, Jan. 1st, 1970 UTC
	public static long getCurrTimeDay(){
		return currTime/(60*60*24);
	}
	
	//is it A.M. or P.M.?
	//true for am, false for pm
	public static boolean isAM(){
		//number of 12-hour periods since Midnight, Jan. 1st, 1970 UTC
		long halfDays = currTime/(60*60*12);
		if(halfDays % 2 == 0){
			return true;
		}
		return false;
	}
	
	//only server should call this
	public static void updateTime(){
		currTime = System.currentTimeMillis()/1000;
	}
	
	//root of the universe
	public static Element getRoot(){
		return rootElement;
	}
	
	public void removeSelf(){
		lookUp.remove(id);
		getAbove().getSubs().remove(this);
	}
	
	public Element getAbove(){
		return above;
	}
	
	//setting above to null indicates that this element is at the top of a hierarchy
	public void setAbove(Element above){
		this.above = above;
	}
	
	public static HashMap<BigInteger, Element> getLookup(){
		return lookUp;
	}
	
	public static void setLookup(HashMap<BigInteger, Element> l){
		lookUp = l;
	}
	
	public void addSub(Element e){
		sub.add(e);
	}
	
	public ArrayList<Element> getSubs(){
		return sub;
	}
	
	//add if not already there
	public boolean addAttribute(String attr, Serializable obj){
		if(!data.containsKey(attr) && obj != null && attr != null){
			return (data.put(attr, obj) != null);
			
		}
		return false;
	}
	
	//returns false if already contained
	public boolean setAttribute(String attr, Serializable newValue){
		if(data.containsKey(attr) && newValue != null && attr != null)
			return (data.put(attr, newValue) != null);
		else
			return false;
	}
	
	//tries to set the attribute. if fails(because it is not set),
	//will add the attribute
	//returns true if added or set, false if neither
	public boolean attr(String attr, Serializable value){
		if(!setAttribute(attr,value)){
			if(!addAttribute(attr, value))
				return false;
		}
		return true;
	}
	
	public static Element getElement(BigInteger id){
		return lookUp.get(id);
	}
	
	//returns null if not found
	public Object getAttribute(String attr){
		return data.get(attr);
	}
	
	//will move into the specified element unless that element is null or this element does not have a name
	public void moveInto(Element e){
		if(e != null){
			getAbove().getSubs().remove(this);
			e.addSub(this);
			setAbove(e);
		}
		else{
			JadeFyre.writeLine("invalid target.");
		}
	}
	
	public Object[] getAttributes(){
		return data.keySet().toArray();
	}
	
	public static Element getById(BigInteger id){
		return lookUp.get(id);
	}
	
	//like getById(starting, id) but staring is the root element
	public static Element getByIdRecursive(BigInteger id){
		return getByIdRecursive(getRoot(), id);
	}
	
	//finds the element starting at the specified element or returns null
	public static Element getByIdRecursive(Element starting, BigInteger id){
		Element search = null;
		if(starting == null){return null;}
		for(Element e : starting.getSubs()){
			if(e.getID().equals(id)){return e;}
			if((search = getByIdRecursive(e,id)) != null){return search;};
		}
		return search;
	}
	
	public void setName(String name){
		attr("name", name);
	}
	
	//returns name or an empty string if it does not have a name
	public String getName(){
		return (getAttribute("name") == null ? "null" : getAttribute("name").toString());
	}
	
	//call it every so often...
	public void update(){
		//check all delayed events
		for(Long tmp : delayedEvents.keySet()){
			Thread t = delayedEvents.get(tmp);
			if(t.getState() == Thread.State.TERMINATED){
				delayedEvents.remove(tmp);
				continue;
			}
			if(tmp.longValue() > System.currentTimeMillis()){
				t.start();
			}
		}
		
		for(Element e : getSubs()){
			e.update();
		}
	}
	
	public boolean equals(Object obj){
		return (obj instanceof Element ? ((Element)obj).id.equals(this.id) : false);
	}
	
	public String toString(){
		return ("'"+getName()+"'"+"["+id.toString()+"]");
	}
	
	public static void setRoot(Element e){
		if(e != null)
			rootElement = e;
	}
	
	public static void setCounter(BigInteger newCntr){
		idCntr = newCntr;
	}
	
	public static BigInteger getCounter(){
		return idCntr;
	}
	
	public void onUse(Element e, String extraInfo){
		useEvent.actionPerformed(new ActionEvent(e, 0, extraInfo));
	}
	
	public static ArrayList<Element> getElementByName(String name){
		ArrayList<Element> output = new ArrayList<Element>();
		for(Element e : lookUp.values()){
			if(e.getName().equals(name)){
				output.add(e);
			}
		}
		return output;
	}
	
	//gets all elements with the specified name in the given element
	public static ArrayList<Element> getElementByName(Element above, String name){
		ArrayList<Element> output = new ArrayList<Element>();
		for(Element e : above.getSubs()){
			if(e.getName().equals(name)){
				output.add(e);
			}
		}
		return output;
	}
	
	public void setUseEvent(Action useEvent) {
		this.useEvent = useEvent;
	}

	public static Element getSpawn() {
		return getById(spawn_id);
	}

	public static void setSpawn(Element s) {
		spawn_id = s.getID();
	}
	
	public static void setSpawn(BigInteger id){
		spawn_id = id;
	}

	public void onSay(Element e, String extraInfo) {
		sayEvent.actionPerformed(new ActionEvent(e, 0, extraInfo));
	}

	public void setSayEvent(Action sayEvent) {
		this.sayEvent = sayEvent;
	}

	public void onEnter(Element e, String extraInfo) {
		enterEvent.actionPerformed(new ActionEvent(e, 0, extraInfo));
	}

	public void setEnterEvent(Action enterEvent) {
		this.enterEvent = enterEvent;
	}
	
	//e is who called this
	public void onLeave(Element e, String extraInfo) {
		leaveEvent.actionPerformed(new ActionEvent(e, 0, extraInfo));
	}

	public void setLeaveEvent(Action leaveEvent) {
		this.leaveEvent = leaveEvent;
	}

	public static long getWorldID() {
		return worldID;
	}

	public static void setWorldID(long worldID) {
		Element.worldID = worldID;
	}
	
	//e is who called this
	public void setDelayEvent(final Element e, final String extraInfo, long milisecondsUntilCalled, final Action delayEvent) {
		delayedEvents.put(new Long(System.currentTimeMillis()+milisecondsUntilCalled), new Thread(new Runnable() {
			
			@Override
			public void run() {
				delayEvent.actionPerformed(new ActionEvent(e, 0, extraInfo));
			}
		}));
	}
}