package core;

import java.math.BigInteger;

import database.DataBase;

public class Generator {
	
	//returns the 'base' element (aka the world)
	public static Element init(){
		Element e = new Element(null);
		e.setName("The Universe");
		e.setAttribute("name", "the universe");
		e.setAttribute("desc", "the start of everything...");
		
		return e;
	}
	
	public static Element createEmptyRoom(){
		Element e = new Element(null);
		e.setName("An empty room");
		e.addAttribute("name", "room");
		e.addAttribute("desc", "An empty room");
		return e;
	}
	
	public static Element CreateNewPlayer(String name){
		BigInteger id = DataBase.getPlayerId(name);
		if(id == null){
			Element p = new Element(Element.getSpawn());
			p.addAttribute("name", name);
			p.addAttribute("player", "true");
			Element.getSpawn().addSub(p);
			DataBase.setPlayerId(name, p.getID());
			return p;
		}
		Element e = Element.getById(id);
		if(e == null){
			Element p = new Element(Element.getSpawn());
			p.addAttribute("name", name);
			p.addAttribute("player", "true");
			Element.getSpawn().addSub(p);
			DataBase.setPlayerId(name, p.getID());
			return p;
		}
		e.attr("hidden", "false");
		DataBase.setPlayerId(name, e.getID());
		return e;
	}
}