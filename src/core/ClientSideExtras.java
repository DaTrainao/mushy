package core;

public class ClientSideExtras{
	
	//type
	public static final int NONE = 0;
	public static final int DARIUS_CLIENT = 1;
	
	//colors
	public static final int DEFAULT_COLOR = 0;
	public static final int BLUE_COLOR = 1;
	public static final int RED_COLOR = 2;
	public static final int GREEN_COLOR = 3;
	
	private int currentType;
	private int currentColor;
	
	public ClientSideExtras(){
		setCurrentType(NONE);
		setCurrentColor(DEFAULT_COLOR);
	}
	
	public ClientSideExtras(int font, int color){
		setCurrentColor(color);
	}
	
	public String formatText(String text){
		return formatAsType(currentType, currentColor, text);
	}
	
	public void setType(int type){
		currentType = type;
	}
	
	public static String formatAsType(int type, int color, String text){
		switch(type){
			case NONE:
				System.out.println("1");
				
				return text;
			case DARIUS_CLIENT:
				String out = "@#$%&_";
				out += "c:[";
				switch(color){
					case DEFAULT_COLOR:
						out += "NULL";
						break;
					case GREEN_COLOR:
						out += "GREEN";
						break;
					case RED_COLOR:
						out += "RED";
						break;
					case BLUE_COLOR:
						out += "BLUE";
						break;
				}
				
				return (out + "]_@#$%&_" + text);
			default:
				return text + "\nFORMATTING ERROR: TYPE NOT FOUND";
		}
	}

	public int getCurrentColor() {
		return currentColor;
	}

	public void setCurrentColor(int currentColor) {
		this.currentColor = currentColor;
	}
	
	public int getCurrentType() {
		return currentType;
	}

	public void setCurrentType(int currentType) {
		this.currentType = currentType;
	}
}