package core;

public class Achievement {
	private String name; //name of achievement
	private String desc; //description of achievement
	private String sendOnCompletion; //text to send user when they complete it
	
	public Achievement(String name){
		this.name = name;
	}
	
	public Achievement(String name, String description){
		this.name = name;
		this.desc = description;
	}
	
	public Achievement(String name, String description, String onComplete){
		this.name = name;
		this.desc = description;
		this.sendOnCompletion = onComplete;
	}

	public String getName() {
		return name;
	}

	public String getDesc() {
		return desc;
	}

	public String getSendOnCompletion() {
		return sendOnCompletion;
	}

}