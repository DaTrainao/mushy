package clients;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.NoSuchElementException;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JFrame;

public class GraphicClient implements Runnable {
	private static JTextArea viewer = new JTextArea();
	private static JTextField input = new JTextField();
	private static JFrame holder = new JFrame();
	private static JScrollPane viewerScroll = new JScrollPane(viewer);
	private static String inputText;
	private static BufferedReader inputFromServer;
	private static WindowListener exitListener;
	
	private static DataOutputStream out;
	private static Socket s;
	
	public static void main(String args[]) throws IOException {
		Init();
		new Thread(new GraphicClient()).start();
	}
	
	private static void Init() throws IOException {
		try {
			s = new Socket(getIP(), getPort());
			out = new DataOutputStream(s.getOutputStream());
			inputFromServer = new BufferedReader(new InputStreamReader(s.getInputStream()));
		} catch (UnknownHostException e) {
			JOptionPane.showMessageDialog(null, "Error Connecting to Server.", "Connection Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
			out.close();
			inputFromServer.close();
			s.close();
			
		} catch (IOException e) {
			e.printStackTrace();
			out.close();
			inputFromServer.close();
			s.close();
		}
		
		
		holder.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		holder.setBounds(100, 100, 800, 400);
		
		//Add Viewer Pane
		holder.add(viewerScroll);
		viewer.setBackground(Color.BLACK);
		viewer.setForeground(Color.WHITE);
		viewer.setLineWrap(true);
		viewer.setAutoscrolls(true);
		viewer.setEditable(false);
		
		//Add Input Box
		holder.add(input, BorderLayout.SOUTH);
		input.setBackground(Color.BLACK);
		input.setForeground(Color.WHITE);
		input.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent e) {
				
			}
			public void keyReleased(KeyEvent e) {
				if(e.getKeyCode() == (KeyEvent.VK_ENTER)) {
					inputText = input.getText();
					input.setText("");
					try {
						sendCommand(inputText);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					//viewer.append(inputText + "\n");
				}
			}
			public void keyTyped(KeyEvent e) {
				
			}
		});
		
		//Create custom Close Operation
		exitListener = new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                try {
					Close();
				} catch (IOException e1) {
					JOptionPane.showMessageDialog(null, "Client did not close properly.", "Shutdown Error", JOptionPane.ERROR_MESSAGE);
					e1.printStackTrace();
				}
            }
        };
        
        holder.addWindowListener(exitListener);
		
		holder.setVisible(true);
	}
	
	private static void Close() throws IOException {
		out.close();
		inputFromServer.close();
		s.close();
	}
	
	private static void sendCommand(String inputText2) throws IOException {
		try { 
			out.writeBytes(inputText2 + "\n");
		} catch (IOException e) { 
			JOptionPane.showMessageDialog(null, "Client could not send text.", "Communication Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
			Close();
		}
	}

	@Override
	public void run() {
		while(true) {
			try {
				if(!inputFromServer.readLine().equalsIgnoreCase(null))
					inputText = inputFromServer.readLine();
			} catch (IOException e) {
				e.printStackTrace();
				try {
					Close();
				} catch (IOException e1) {
					e1.printStackTrace();
					System.exit(0);
				}
			}
			viewer.append(inputText + "\n");
		}
	}
	
	private static String getIP() {
		return JOptionPane.showInputDialog("Enter Server IP");
	}
	
	private static int getPort() {
		return Integer.valueOf(JOptionPane.showInputDialog("Enter Server Port"));
	}
	
}