package clients;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;
import javax.swing.JApplet;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class SimpleWebClient extends JApplet implements Runnable{
	private static final long serialVersionUID = 1L;
	
	private Socket s;
	private static DataOutputStream out;
	private static BufferedReader in;
	private static boolean connected = false;
	private static JPanel panel;
	private static JTextArea output;
	private static JTextField input;
	
	public void init(){
		
		try {
			panel = new JPanel();
			output = new JTextArea();
			input = new JTextField();
			output.setEditable(false);
			input.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					try {
						out.writeBytes(input.getText());
						input.setText("");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			input.setVisible(true);
			output.setVisible(true);
			panel.add(output);
			panel.add(input);
			panel.setEnabled(true);
			panel.setVisible(true);
			getContentPane().add(panel);
			
			String ip = JOptionPane.showInputDialog(null, "Enter Server's IP Address:");
			Integer port = Integer.valueOf(JOptionPane.showInputDialog(null, "Enter Server's Port:"));
			s = new Socket(ip, port);
			out = new DataOutputStream(s.getOutputStream());
			in = new BufferedReader(new InputStreamReader(s.getInputStream()));
			connected = true;
			
		} catch (UnknownHostException e) {
			connected = false;
		} catch (IOException e) {
			connected = false;
		}
		if(connected){
			output.append("connected to server.");
		}
		else{
			output.append("could not connect to server.");
		}
	}
	
	public void paint(Graphics g){
		repaint();
	}

	@Override
	public void run() {
		while(true){
			try {
				output.append(in.readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}