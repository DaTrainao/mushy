package clients;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

//similar to simpleClient, but automatically connects to localhost on port 1338
public class TestingClient implements Runnable {
	
	private static DataOutputStream out;
	private static BufferedReader in;
	private static boolean running = true;
	private static Socket s;
	
	public static void main(String args[]){
		try{
			try {
				s = new Socket("localhost",1338);
				out = new DataOutputStream(s.getOutputStream());
				in = new BufferedReader(new InputStreamReader(s.getInputStream()));
				
				new Thread(new TestingClient()).start();
				while(running){
					System.out.println(in.readLine());
				}
				
				out.close();
				in.close();
				s.close();
			} catch (UnknownHostException e) {
				e.printStackTrace();
				out.close();
				in.close();
				s.close();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				out.close();
				in.close();
				s.close();
			}
		}
		catch(Exception e){
			e.printStackTrace();
			System.exit(0);
		}
		System.exit(0);
	}

	@Override
	public void run() {
		Scanner sysIn = new Scanner(System.in);
		String userIn = "";
		try {
			out.writeBytes("connect chris pass\n");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		while(running) {
			try {
				userIn = sysIn.nextLine();
				out.writeBytes(userIn + "\n");
				if(userIn.equals("quit")){
					running=false;
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		running = false;
		try {
			out.close();
			in.close();
			s.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.exit(0);
	}
}