package clients;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SpinnerNumberModel;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultCaret;
import javax.swing.text.Document;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

public class Client extends JFrame implements Runnable {
	private static final long serialVersionUID = 1L;
	private JDesktopPane desktop = new JDesktopPane();
	private JTextPane output, map;
	private Window outputWin, mapWin;
	private JTextField input = new JTextField();
	private WindowAdapter exitListener;
	private DefaultCaret scrollDown;
	
	private BufferedReader in;
	private DataOutputStream out;
	private Socket s;
	private String text;
	
	public Client() throws IOException {
		super("JadeFyre - Mushy++ Client");
		init();
	}
	
	public void init() throws IOException {
		//Connect to Server, preset: ("localhost", 1338)
		try {
			s = new Socket("localhost", 1338);
			out = new DataOutputStream(s.getOutputStream());
			in = new BufferedReader(new InputStreamReader(s.getInputStream()));
		} catch (UnknownHostException e) {
			JOptionPane.showMessageDialog(null, "Error Connecting to Server.", "Connection Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
			out.close();
			in.close();
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
			out.close();
			in.close();
			s.close();
		}
		
		//define presets
		output = new JTextPane();
		output.setBackground(Color.BLACK);
		output.setForeground(Color.WHITE);
		output.setEditable(false);
		scrollDown = (DefaultCaret) output.getCaret();
		scrollDown.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		
		map = new JTextPane();
		map.setBackground(Color.BLACK);
		map.setForeground(Color.WHITE);
		map.setEditable(false);
		
		//create JInternalFrames
		outputWin = new Window("Output",0,0,580,380);
		outputWin.add(output);
		outputWin.setVisible(true);
		outputWin.setBorder(null);
		outputWin.setClosable(false);
		
		mapWin = new Window("Map",580,100,380,394);
		mapWin.add(map);
		mapWin.setVisible(true);
		mapWin.setClosable(false);
		
		//create virtual desktop
		desktop.add(mapWin);
		desktop.add(outputWin);
		desktop.setBackground(Color.BLACK);
		input.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent e) {
				
			}
			public void keyReleased(KeyEvent e) {
				if(e.getKeyCode() == (KeyEvent.VK_ENTER)) {
					text = input.getText();
					input.setText("");
					try {
						sendCommand(text);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					print(text);
				}
			}
			public void keyTyped(KeyEvent e) {
				
			}
		});
		
		//pack and display
		add(desktop);
		add(input, BorderLayout.SOUTH);
		
		//create custom close operation
		exitListener = new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                try {
					Close();
				} catch (IOException e1) {
					JOptionPane.showMessageDialog(null, "Client did not close properly.", "Shutdown Error", JOptionPane.ERROR_MESSAGE);
					e1.printStackTrace();
				}
            }
        };
		
		//setContentPane(desktop);
		setVisible(true);
		setSize(800, 400);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		addWindowListener(exitListener);
	}
	
	private void sendCommand(String inputText2) throws IOException {
		try { 
			out.writeBytes(inputText2 + "\n");
		} catch (IOException e) { 
			JOptionPane.showMessageDialog(null, "Client could not send text.", "Communication Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
	
	private void Close() throws IOException {
		out.close();
		in.close();
		s.close();
	}
	
	@Override
	public void run() {
		while(true) {
			try {
				if(!in.readLine().equalsIgnoreCase(null)) {
					text = in.readLine();
					this.println(output, text);
				}
			} catch(IOException e) {
				
			}
		}
		
	}
	
	private void println(JEditorPane pane, String text) {
		try {
			Document doc = pane.getDocument();
			doc.insertString(doc.getLength(), (text + "\n"), null);
			pane.setCaretPosition(doc.getLength());
		} catch(BadLocationException e) {
			JOptionPane.showMessageDialog(null, "Error sending to server.", "Connection Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void print(JTextPane tp, String msg, Color c) {
		StyledDocument doc = tp.getStyledDocument();

        Style style = tp.addStyle("I'm a Style", null);
        StyleConstants.setForeground(style, c);

        try {
            doc.insertString(doc.getLength(), "BLAH ", style);
        } catch (BadLocationException ex) {
        }

        StyleConstants.setForeground(style, Color.blue);

        try {
            doc.insertString(doc.getLength(), "BLEH", style);
        } catch (BadLocationException e) {
        }
    }
	
	private void print(String text) {
		String temp;
		String color;
		String[] tempSplit = null;
		if(text.contains("ansi(")) {
			for(int k = text.indexOf("ansi("); k < text.length(); k++) {
				if(text.charAt(k) == ')') {
					print(output, text.substring(0, text.indexOf("ansi(")), Color.WHITE);
					temp = text.substring(text.indexOf("ansi("), k);
					//print in color!!
					temp.replace(')', ' ');
					temp.trim();
					print(output, temp.substring(5, temp.length()), getColor(temp));
					//end color print!
					text.replaceFirst("ansi(", "");
					tempSplit = text.split(" ");
				}
			}
		}
		
	}
	
	private Color getColor(String color) {
		switch(color) {
		case "b":
			return Color.BLUE;
		case "r":
			return Color.RED;
		case "w":
			return Color.WHITE;
		case "c":
			return Color.CYAN;
		case "G":
			return Color.GRAY;
		case "g":
			return Color.GREEN;
		case "m":
			return Color.MAGENTA;
		case "o":
			return Color.ORANGE;
		case "p":
			return Color.PINK;
		case "y":
			return Color.YELLOW;
		default:
			return null;
		}
	}
	
	public static void main(String[] args) throws IOException {
		new Thread(new Client()).start();
	}
	
}

class Window extends JInternalFrame {
	private static final long serialVersionUID = 1L;

	public Window(String name) {
		super(name, true, // resizable
				true, // closable
				true, // maximizable
				false);// iconifiable
		setSize(400, 200);
		setLocation(0,0);
	}
	
	public Window(String name, int x, int y) {
		super(name, true, // resizable
				true, // closable
				true, // maximizable
				false);// iconifiable
		setSize(300, 300);
		setLocation(x, y);
	}
	
	public Window(String name, int x, int y, int width, int height) {
		super(name, true, // resizable
				true, // closable
				true, // maximizable
				false);// iconifiable
		setSize(width, height);
		setLocation(x, y);
	}
	
	public Window(String name, int x, int y, int width, int height, boolean resizable, boolean closable, boolean maximizable) {
		super(name, resizable, // resizable
				closable, // closable
				maximizable, // maximizable
				false);// iconifiable
		setSize(width, height);
		setLocation(x, y);
	}
	
	public Window(String name, int x, int y, int width, int height, boolean resizable, boolean closable, boolean maximizable, boolean iconifiable) {
		super(name, resizable, // resizable
				closable, // closable
				maximizable, // maximizable
				iconifiable);// iconifiable
		setSize(width, height);
		setLocation(x, y);
	}
	
}

class MenuBar extends JMenuBar {
	private static final long serialVersionUID = 1L;

	public MenuBar() {
		
		JMenu connectOptions = new JMenu("Connect");
		
		JMenuItem connectTo = new JMenuItem("Connect to...");
		connectOptions.add(connectTo);
		
		add(connectOptions);
		
		JMenu formattingOptions = new JMenu("Formatting");
		
		add(formattingOptions);
	}
	//JMenu
	public void createMenu() {
		
	}
	//JMenuOption - Menu Options
	public void addMenuItem(JMenu m) {
		
	}
}