package serverSideCommands;

import core.BasicServerCommand;
import core.Element;
import core.JadeFyre;

public class ListAllWorldObjects extends BasicServerCommand{

	public ListAllWorldObjects() {
		super("listObjects", "list all world objects");
	}

	@Override
	public void doCmd(String extraInfo) {
		System.out.println(compileObjects(Element.getRoot(),0));
	}
	private String compileObjects(Element e,int numSpaces){
		String compiled = "";
		String spaces = "| ";
		for(int k = 0; k < numSpaces; k++)
			spaces+="| ";
		compiled += e.getName()+"["+e.getID()+"]\n";
		for(Element tmpE : e.getSubs()){
			compiled += spaces+compileObjects(tmpE,numSpaces + 1)+"";
		}
		return compiled;
	}
}