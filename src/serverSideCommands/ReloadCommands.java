package serverSideCommands;

import core.BasicServerCommand;
import core.JadeFyre;

public class ReloadCommands extends BasicServerCommand {

	public ReloadCommands() {
		super("reloadCommands", "reload all commands");
	}

	@Override
	public void doCmd(String extraInfo) {
		JadeFyre.initCmds();
		JadeFyre.initServerSideCommands();
	}

}
