package serverSideCommands;

import core.BasicServerCommand;
import core.ClientInstance;
import core.JadeFyre;

public class ListLoggedOn extends BasicServerCommand {

	public ListLoggedOn() {
		super("list", "lists logged on users in format 'ip_address  player_name'");
	}

	@Override
	public void doCmd(String extraInfo) {
		System.out.println("--LOGGED ON USERS--");
		for(ClientInstance tmp: JadeFyre.getLoggedOnClients()){
			System.out.println(tmp.getSocket().getInetAddress().toString() + "   "  + tmp.getPlayer().getName());
		}
		System.out.println("-------------------");
	}
	
	public static void main(String[] args) {
		
	}
}
