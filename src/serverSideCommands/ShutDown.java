package serverSideCommands;

import core.BasicServerCommand;
import core.JadeFyre;

public class ShutDown extends BasicServerCommand {
	
	public ShutDown() {
		super("shutdown", "saves world, disconnects clients, and stops server");
	}
	
	@Override
	public void doCmd(String extraInfo) {
		JadeFyre.shutDown();
		System.exit(0);
	}
}