package serverSideCommands;

import java.io.IOException;

import core.BasicServerCommand;
import core.JadeFyre;
import database.DataBase;

public class SaveWorld extends BasicServerCommand{

	public SaveWorld() {
		super("save", "save world to file");
	}
	
	@Override
	public void doCmd(String extraInfo) {
		if(!DataBase.saveWorld())
			System.out.println("error saving!");
		System.out.println("saved!");
	}

}
