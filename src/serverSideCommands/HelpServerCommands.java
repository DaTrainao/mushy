package serverSideCommands;

import core.BasicServerCommand;
import core.JadeFyre;

public class HelpServerCommands extends BasicServerCommand {

	public HelpServerCommands() {
		super("help", "shows help");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void doCmd(String extraInfo) {
		if(extraInfo.equals("")){
			System.out.println("-SERVER COMMANDS-");
			for(BasicServerCommand c : JadeFyre.getServerCommands()){
				System.out.println(c.getName() + " : " + c.getDescription());
			}
			System.out.println("-----------------");
		}
		else{
			String temp = "";
			for(int i = 0; i < extraInfo.length(); i ++){
				temp += "-";
			}
			System.out.println("-"+extraInfo+"-");
			System.out.println(JadeFyre.getCommandByName(extraInfo).getDescription());
			System.out.println("-"+temp+"-");
		}
	}

}
