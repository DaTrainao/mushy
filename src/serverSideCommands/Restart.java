package serverSideCommands;

import core.BasicServerCommand;
import core.JadeFyre;

public class Restart extends BasicServerCommand{
	public Restart(){
		super("restart","restarts server");
	}

	@Override
	public void doCmd(String extraInfo) {
		JadeFyre.restart();
	}
}