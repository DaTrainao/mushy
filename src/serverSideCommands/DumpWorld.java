package serverSideCommands;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import core.BasicServerCommand;
import core.Element;
import core.JadeFyre;

public class DumpWorld extends BasicServerCommand {

	public DumpWorld() {
		super("dump", "exports world relationships to \'data/world/world_dump.txt\'");
	}

	@Override
	public void doCmd(String extraInfo) {
		try{
			File f = new File("data/world/world_dump.txt");
			if(!f.exists())
				f.createNewFile();
			BufferedWriter out = new BufferedWriter(new FileWriter(f));
			dump(Element.getRoot(), out,0);
			out.close();
		}catch(Exception e){
			JadeFyre.writeLine("There was an error");
		}
	}
	
	private void dump(Element e, BufferedWriter out,int numIn) throws IOException{
		String in = "";
		for(int i = 0; i < numIn; i ++){
			in += "|";
		}
		out.write(in+e.toString());
		out.newLine();
		for(Element tmpE : e.getSubs()){
			dump(tmpE,out,numIn+1);
		}
		
	}
}