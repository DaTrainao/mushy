package serverSideCommands;

import graphics.WorldGUIPanel;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JFrame;

import WorldGen.MiscHelperMethods;
import core.BasicServerCommand;

public class GUIWorld extends BasicServerCommand {
	public GUIWorld() {
		super("gui", "Opens a window so you can see a graphical representation of the world");
	}
	 @Override
	public void doCmd(String extraInfo) {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		Dimension scr = Toolkit.getDefaultToolkit().getScreenSize();
		Image img = MiscHelperMethods.getWorldImage();
		Dimension worldSize = new Dimension(img.getWidth(null),img.getHeight(null));
		frame.setPreferredSize(new Dimension(worldSize.width*2, worldSize.height*2));
		frame.setLocation(scr.width/2 - worldSize.width/2, scr.height/2 - worldSize.height/2);
		frame.setResizable(true);
		frame.setLayout(new BorderLayout());
		frame.add(BorderLayout.CENTER,new WorldGUIPanel());
		frame.pack();
		frame.setVisible(true);
		frame.setFocusable(true);
		frame.requestFocus();
		frame.toFront();
		frame.repaint();
	}
}